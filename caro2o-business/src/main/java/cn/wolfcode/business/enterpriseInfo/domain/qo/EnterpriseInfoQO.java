package cn.wolfcode.business.enterpriseInfo.domain.qo;

import cn.wolfcode.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.util.Calendar;
import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class EnterpriseInfoQO {
    private Long id;

    private String keyword;

    /** 所属地区 */
    @Excel(name = "所属地区")
    private Integer affiliatingArea;

    /** 经营状态 */
    @Excel(name = "经营状态")
    private Integer operatingStatus;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;

    public Date getEndTime(){
        Calendar calendar = Calendar.getInstance();
        if(this.endTime != null){
            calendar.setTime(this.endTime);
            calendar.add(Calendar.DAY_OF_MONTH,1);
            return calendar.getTime();
        }
        return null;
    }
}
