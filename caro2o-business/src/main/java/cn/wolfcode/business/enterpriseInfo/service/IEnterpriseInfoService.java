package cn.wolfcode.business.enterpriseInfo.service;

import java.util.List;
import cn.wolfcode.business.enterpriseInfo.domain.EnterpriseInfo;
import cn.wolfcode.business.enterpriseInfo.domain.qo.EnterpriseInfoQO;
import cn.wolfcode.business.enterpriseInfo.domain.qo.EnterpriseInfoVO;

/**
 * 客户基本信息Service接口
 * 
 * @author taotao
 * @date 2023-06-16
 */
public interface IEnterpriseInfoService 
{
    /**
     * 查询客户基本信息
     * 
     * @param id 客户基本信息主键
     * @return 客户基本信息
     */
    public EnterpriseInfo selectEnterpriseInfoById(Long id);

    /**
     * 查询客户基本信息列表
     * 
     * @param enterpriseInfo 客户基本信息
     * @return 客户基本信息集合
     */
    public List<EnterpriseInfo> selectEnterpriseInfoList(EnterpriseInfoQO enterpriseInfo);

    /**
     * 新增客户基本信息
     * 
     * @param enterpriseInfo 客户基本信息
     * @return 结果
     */
    public int insertEnterpriseInfo(EnterpriseInfoVO enterpriseInfo);

    /**
     * 修改客户基本信息
     * 
     * @param enterpriseInfo 客户基本信息
     * @return 结果
     */
    public int updateEnterpriseInfo(EnterpriseInfoVO enterpriseInfo);

    /**
     * 批量删除客户基本信息
     * 
     * @param ids 需要删除的客户基本信息主键集合
     * @return 结果
     */
    public int deleteEnterpriseInfoByIds(Long[] ids);

    /**
     * 删除客户基本信息信息
     * 
     * @param id 客户基本信息主键
     * @return 结果
     */
    public int deleteEnterpriseInfoById(Long id);

    public List<EnterpriseInfo> getEnterpriseDetailsById(Long id);

    List<EnterpriseInfo> selectEnterpriseInfoAllList();


}
