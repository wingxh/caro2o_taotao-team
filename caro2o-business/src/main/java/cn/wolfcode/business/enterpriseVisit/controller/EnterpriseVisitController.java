package cn.wolfcode.business.enterpriseVisit.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import cn.wolfcode.business.contacts.domain.EnterpriseContacts;
import cn.wolfcode.business.contacts.service.IEnterpriseContactsService;
import cn.wolfcode.business.enterpriseInfo.domain.EnterpriseInfo;
import cn.wolfcode.business.enterpriseVisit.domain.vo.EnterpriseVisitVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.wolfcode.common.annotation.Log;
import cn.wolfcode.common.core.controller.BaseController;
import cn.wolfcode.common.core.domain.AjaxResult;
import cn.wolfcode.common.enums.BusinessType;
import cn.wolfcode.business.enterpriseVisit.domain.EnterpriseVisit;
import cn.wolfcode.business.enterpriseVisit.service.IEnterpriseVisitService;
import cn.wolfcode.common.utils.poi.ExcelUtil;
import cn.wolfcode.common.core.page.TableDataInfo;

/**
 * 客户拜访Controller
 * 
 * @author taotao
 * @date 2023-06-17
 */
@RestController
@RequestMapping("/business/enterpriseVisit")
public class EnterpriseVisitController extends BaseController
{
    @Autowired
    private IEnterpriseVisitService enterpriseVisitService;
    @Autowired
    private IEnterpriseContactsService enterpriseContactsService;
    /**
     * 查询客户拜访列表
     */
    @PreAuthorize("@ss.hasPermi('business:enterpriseVisit:list')")
    @GetMapping("/list")
    public TableDataInfo list(EnterpriseVisit enterpriseVisit)
    {
        startPage();
        List<EnterpriseVisit> list = enterpriseVisitService.selectEnterpriseVisitList(enterpriseVisit);
        return getDataTable(list);
    }

    @PreAuthorize("@ss.hasPermi('business:contacts:getContectsList')")
    @GetMapping("/getContectsList")
    public TableDataInfo getContectsList()
    {
        List<EnterpriseContacts> list = enterpriseVisitService.getContectsList();
        return getDataTable(list);
    }
    @PreAuthorize("@ss.hasPermi('business:contacts:getEnterpriseInfoList')")
    @GetMapping("/getEnterpriseInfoList")
    public TableDataInfo getEnterpriseInfoList()
    {
        List<EnterpriseInfo> list = enterpriseVisitService.getEnterpriseInfoList();
        return getDataTable(list);
    }
    /**
     * 获取客户拜访详细信息
     */
    @PreAuthorize("@ss.hasPermi('business:enterpriseVisit:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(enterpriseVisitService.selectEnterpriseVisitById(id));
    }

    /**
     * 新增客户拜访
     */
    @PreAuthorize("@ss.hasPermi('business:enterpriseVisit:add')")
    @Log(title = "客户拜访", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EnterpriseVisitVO enterpriseVisit)
    {
        return toAjax(enterpriseVisitService.insertEnterpriseVisit(enterpriseVisit));
    }

    /**
     * 修改客户拜访
     */
    @PreAuthorize("@ss.hasPermi('business:enterpriseVisit:edit')")
    @Log(title = "客户拜访", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EnterpriseVisitVO enterpriseVisit)
    {
        return toAjax(enterpriseVisitService.updateEnterpriseVisit(enterpriseVisit));
    }

    /**
     * 删除客户拜访
     */
    @PreAuthorize("@ss.hasPermi('business:enterpriseVisit:remove')")
    @Log(title = "客户拜访", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(enterpriseVisitService.deleteEnterpriseVisitByIds(ids));
    }
}
