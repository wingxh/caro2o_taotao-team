package cn.wolfcode.business.packageAudit.domain.vo;

import cn.wolfcode.common.annotation.Excel;
import cn.wolfcode.common.core.domain.BaseEntity;
import lombok.*;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BusCarPackageAuditVO extends BaseEntity
{
    private Long id;
    private Integer auditOpinion;
    private String auditInfo;
}
