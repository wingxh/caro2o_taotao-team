package cn.wolfcode.business.serviceItem.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import cn.wolfcode.business.serviceItem.domain.vo.BusAuditInfo;
import cn.wolfcode.business.serviceItem.domain.vo.BusServiceItemDTO;
import cn.wolfcode.business.serviceItem.domain.vo.BusServiceItemVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.wolfcode.common.annotation.Log;
import cn.wolfcode.common.core.controller.BaseController;
import cn.wolfcode.common.core.domain.AjaxResult;
import cn.wolfcode.common.enums.BusinessType;
import cn.wolfcode.business.serviceItem.domain.BusServiceItem;
import cn.wolfcode.business.serviceItem.service.IBusServiceItemService;
import cn.wolfcode.common.utils.poi.ExcelUtil;
import cn.wolfcode.common.core.page.TableDataInfo;

/**
 * 服务项Controller
 * 
 * @author wolfcode
 * @date 2023-05-31
 */
@RestController
@RequestMapping("/business/serviceItem")
public class BusServiceItemController extends BaseController
{
    @Autowired
    private IBusServiceItemService busServiceItemService;

    /**
     * 查询服务项列表
     */
    @PreAuthorize("@ss.hasPermi('business:serviceItem:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusServiceItem busServiceItem)
    {
        startPage();
        List<BusServiceItem> list = busServiceItemService.selectBusServiceItemList(busServiceItem);
        return getDataTable(list);
    }

    /**
     * 获取服务项详细信息
     */
    @PreAuthorize("@ss.hasPermi('business:serviceItem:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(busServiceItemService.selectBusServiceItemById(id));
    }

    /**
     * 新增服务项
     */
    @PreAuthorize("@ss.hasPermi('business:serviceItem:add')")
    @PostMapping
    public AjaxResult add(@RequestBody BusServiceItemVO busServiceItemVO)
    {
        return toAjax(busServiceItemService.insertBusServiceItem(busServiceItemVO));
    }

    /**
     * 修改服务项
     */
    @PreAuthorize("@ss.hasPermi('business:serviceItem:edit')")
    @PutMapping
    public AjaxResult edit(@RequestBody BusServiceItemVO busServiceItemVO)
    {
        return toAjax(busServiceItemService.updateBusServiceItem(busServiceItemVO));
    }

    /**
     * 上架服务项
     */
    @PreAuthorize("@ss.hasPermi('business:serviceItem:saleOn')")
	@PutMapping("/saleOn/{id}")
    public AjaxResult saleOn(@PathVariable Long id)
    {
        return toAjax(busServiceItemService.saleOn(id));
    }

    /**
     * 下架服务项
     */
    @PreAuthorize("@ss.hasPermi('business:serviceItem:saleOff')")
    @PutMapping("/saleOff/{id}")
    public AjaxResult saleOff(@PathVariable Long id)
    {
        return toAjax(busServiceItemService.saleOff(id));
    }
    /**
     * 查询服务项数据
     */
    @PreAuthorize("@ss.hasPermi('business:serviceItem:queryAuditInfo')")
    @GetMapping("/queryAuditInfo/{id}")
    public AjaxResult queryAuditInfo(@PathVariable Long id)
    {
        BusAuditInfo busAuditInfo = busServiceItemService.queryBusAuditInfoByServiceItemId(id);
        return AjaxResult.success(busAuditInfo);
    }
    /**
     * 查询服务项数据
     */
    @PreAuthorize("@ss.hasPermi('business:serviceItem:startAudit')")
    @PostMapping("/startAudit")
    public AjaxResult startAudit(@RequestBody BusServiceItemDTO dto)
    {
        return toAjax(busServiceItemService.startAudit(dto));
    }
}
