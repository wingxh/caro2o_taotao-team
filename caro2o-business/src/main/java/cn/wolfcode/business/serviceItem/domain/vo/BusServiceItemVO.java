package cn.wolfcode.business.serviceItem.domain.vo;

import cn.wolfcode.common.annotation.Excel;
import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BusServiceItemVO {

    /** $column.columnComment */
    private Long id;

    /** 服务名称 */
    @Excel(name = "服务名称")
    private String name;

    /** 服务原价 */
    @Excel(name = "服务原价")
    private BigDecimal originalPrice;

    /** 折扣价 */
    @Excel(name = "折扣价")
    private BigDecimal discountPrice;

    /** 是否套餐 */
    @Excel(name = "是否套餐")
    private Integer carPackage;

    /** 备注信息 */
    @Excel(name = "备注信息")
    private String info;

    /** 服务分类 */
    @Excel(name = "服务分类")
    private Integer serviceCatalog;


}
