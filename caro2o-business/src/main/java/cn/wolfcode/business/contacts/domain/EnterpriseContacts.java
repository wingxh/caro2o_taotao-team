package cn.wolfcode.business.contacts.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import cn.wolfcode.common.annotation.Excel;
import cn.wolfcode.common.core.domain.BaseEntity;

/**
 * 联系人对象 enterprise_contacts
 * 
 * @author 韬韬
 * @date 2023-06-17
 */
public class EnterpriseContacts extends BaseEntity
{
    public static final Integer ENPLOYMENNT_STATUS_ON = 0;//在职
    public static final Integer ENPLOYMENNT_STATUS_OFF = 1;//离职
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 企业名称 */
    @Excel(name = "企业名称")
    private String enterpriseName;

    /** 联系人名字 */
    @Excel(name = "联系人名字")
    private String contacts;

    /** 性别 */
    @Excel(name = "性别")
    private Integer gender;

    /** 年龄 */
    @Excel(name = "年龄")
    private Integer age;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String contactPhone;

    /** 职位 */
    @Excel(name = "职位")
    private String position;

    /** 部门 */
    @Excel(name = "部门")
    private String department;

    /** 任职状态 */
    @Excel(name = "任职状态")
    private Integer enploymentStatus;

    /** 录入人 */
    @Excel(name = "录入人")
    private String clerk;

    /** 录入时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "录入时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date entryTime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setEnterpriseName(String enterpriseName) 
    {
        this.enterpriseName = enterpriseName;
    }

    public String getEnterpriseName() 
    {
        return enterpriseName;
    }
    public void setContacts(String contacts) 
    {
        this.contacts = contacts;
    }

    public String getContacts() 
    {
        return contacts;
    }
    public void setGender(Integer gender) 
    {
        this.gender = gender;
    }

    public Integer getGender() 
    {
        return gender;
    }
    public void setAge(Integer age) 
    {
        this.age = age;
    }

    public Integer getAge() 
    {
        return age;
    }
    public void setContactPhone(String contactPhone) 
    {
        this.contactPhone = contactPhone;
    }

    public String getContactPhone() 
    {
        return contactPhone;
    }
    public void setPosition(String position) 
    {
        this.position = position;
    }

    public String getPosition() 
    {
        return position;
    }
    public void setDepartment(String department) 
    {
        this.department = department;
    }

    public String getDepartment() 
    {
        return department;
    }
    public void setEnploymentStatus(Integer enploymentStatus) 
    {
        this.enploymentStatus = enploymentStatus;
    }

    public Integer getEnploymentStatus() 
    {
        return enploymentStatus;
    }
    public void setClerk(String clerk) 
    {
        this.clerk = clerk;
    }

    public String getClerk() 
    {
        return clerk;
    }
    public void setEntryTime(Date entryTime) 
    {
        this.entryTime = entryTime;
    }

    public Date getEntryTime() 
    {
        return entryTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("enterpriseName", getEnterpriseName())
            .append("contacts", getContacts())
            .append("gender", getGender())
            .append("age", getAge())
            .append("contactPhone", getContactPhone())
            .append("position", getPosition())
            .append("department", getDepartment())
            .append("enploymentStatus", getEnploymentStatus())
            .append("clerk", getClerk())
            .append("entryTime", getEntryTime())
            .toString();
    }
}
