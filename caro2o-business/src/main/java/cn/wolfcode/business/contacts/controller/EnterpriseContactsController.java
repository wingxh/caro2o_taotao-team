package cn.wolfcode.business.contacts.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import cn.wolfcode.business.contacts.domain.vo.EnterpriseContactsQO;
import cn.wolfcode.business.contacts.domain.vo.EnterpriseContactsVO;
import cn.wolfcode.business.enterpriseInfo.domain.EnterpriseInfo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.wolfcode.common.annotation.Log;
import cn.wolfcode.common.core.controller.BaseController;
import cn.wolfcode.common.core.domain.AjaxResult;
import cn.wolfcode.common.enums.BusinessType;
import cn.wolfcode.business.contacts.domain.EnterpriseContacts;
import cn.wolfcode.business.contacts.service.IEnterpriseContactsService;
import cn.wolfcode.common.utils.poi.ExcelUtil;
import cn.wolfcode.common.core.page.TableDataInfo;

/**
 * 联系人Controller
 * 
 * @author 韬韬
 * @date 2023-06-17
 */
@RestController
@RequestMapping("/business/contacts")
public class EnterpriseContactsController extends BaseController
{
    @Autowired
    private IEnterpriseContactsService enterpriseContactsService;

    /**
     * 查询联系人列表
     */
    @PreAuthorize("@ss.hasPermi('business:contacts:list')")
    @GetMapping("/list")
    public TableDataInfo list(EnterpriseContactsQO enterpriseContacts)
    {
        startPage();
        List<EnterpriseContacts> list = enterpriseContactsService.selectEnterpriseContactsList(enterpriseContacts);
        return getDataTable(list);
    }
    @PreAuthorize("@ss.hasPermi('business:contacts:getEnterpriseNameList')")
    @GetMapping("/getEnterpriseNameList")
    public TableDataInfo getEnterpriseNameList()
    {
        List<EnterpriseInfo> list = enterpriseContactsService.getEnterpriseNameList();
        return getDataTable(list);
    }

    /**
     * 获取联系人详细信息
     */
    @PreAuthorize("@ss.hasPermi('business:contacts:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(enterpriseContactsService.selectEnterpriseContactsById(id));
    }

    /**
     * 新增联系人
     */
    @PreAuthorize("@ss.hasPermi('business:contacts:add')")
    @Log(title = "联系人", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EnterpriseContactsVO enterpriseContacts)
    {
        return toAjax(enterpriseContactsService.insertEnterpriseContacts(enterpriseContacts));
    }

    /**
     * 修改联系人
     */
    @PreAuthorize("@ss.hasPermi('business:contacts:edit')")
    @Log(title = "联系人", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EnterpriseContactsVO enterpriseContacts)
    {
        return toAjax(enterpriseContactsService.updateEnterpriseContacts(enterpriseContacts));
    }

    /**
     * 删除联系人
     */
    @PreAuthorize("@ss.hasPermi('business:contacts:remove')")
    @Log(title = "联系人", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(enterpriseContactsService.deleteEnterpriseContactsByIds(ids));
    }
}
