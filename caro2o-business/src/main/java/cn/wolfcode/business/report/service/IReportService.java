package cn.wolfcode.business.report.service;

import cn.wolfcode.business.report.domain.vo.CustomerReportQO;
import cn.wolfcode.business.report.domain.vo.CustomerReportVO;
import cn.wolfcode.business.report.domain.vo.ShopReportQO;
import cn.wolfcode.business.report.domain.vo.ShopReportVO;

import java.util.List;

public interface IReportService {
    public List<ShopReportVO> queryShopReport(ShopReportQO qo);

    public List<CustomerReportVO> queryCustomerReport(CustomerReportQO qo);
}
