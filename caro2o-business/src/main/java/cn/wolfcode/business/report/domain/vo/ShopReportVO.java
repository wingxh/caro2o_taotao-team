package cn.wolfcode.business.report.domain.vo;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ShopReportVO {
    private String payTime;
    private String totalCount;
    private String totalAmount;
}
