package cn.wolfcode.business.report.domain.vo;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CustomerReportVO {
    private String customerName;
    private String customerPhone;
    private String totalAmount;
}
