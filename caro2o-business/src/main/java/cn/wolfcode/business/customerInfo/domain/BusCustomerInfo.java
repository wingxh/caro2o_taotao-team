package cn.wolfcode.business.customerInfo.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import cn.wolfcode.common.annotation.Excel;
import cn.wolfcode.common.core.domain.BaseEntity;

/**
 * 客户档案对象 bus_customer_info
 * 
 * @author wolfcode
 * @date 2023-06-10
 */
public class BusCustomerInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 客户姓名 */
    @Excel(name = "客户姓名")
    private String customerName;

    /** 电话 */
    @Excel(name = "电话")
    private String customerPhone;

    /** 性别 */
    @Excel(name = "性别")
    private Integer gender;

    /** 录入人 */
    @Excel(name = "录入人")
    private String clerk;

    /** 录入时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "录入时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date enterTime;

    /** $column.columnComment */
    private Long appointmentId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCustomerName(String customerName) 
    {
        this.customerName = customerName;
    }

    public String getCustomerName() 
    {
        return customerName;
    }
    public void setCustomerPhone(String customerPhone) 
    {
        this.customerPhone = customerPhone;
    }

    public String getCustomerPhone() 
    {
        return customerPhone;
    }
    public void setGender(Integer gender) 
    {
        this.gender = gender;
    }

    public Integer getGender() 
    {
        return gender;
    }
    public void setClerk(String clerk) 
    {
        this.clerk = clerk;
    }

    public String getClerk() 
    {
        return clerk;
    }
    public void setEnterTime(Date enterTime) 
    {
        this.enterTime = enterTime;
    }

    public Date getEnterTime() 
    {
        return enterTime;
    }
    public void setAppointmentId(Long appointmentId) 
    {
        this.appointmentId = appointmentId;
    }

    public Long getAppointmentId() 
    {
        return appointmentId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("customerName", getCustomerName())
            .append("customerPhone", getCustomerPhone())
            .append("gender", getGender())
            .append("clerk", getClerk())
            .append("enterTime", getEnterTime())
            .append("appointmentId", getAppointmentId())
            .toString();
    }
}
