package cn.wolfcode.business.customerInfo.service;

import java.util.List;
import cn.wolfcode.business.customerInfo.domain.BusCustomerInfo;
import cn.wolfcode.business.customerInfo.domain.vo.BusCustomerInfoQO;
import cn.wolfcode.business.customerInfo.domain.vo.BusCustomerInfoVO;

/**
 * 客户档案Service接口
 * 
 * @author wolfcode
 * @date 2023-06-10
 */
public interface IBusCustomerInfoService 
{
    /**
     * 查询客户档案
     * 
     * @param id 客户档案主键
     * @return 客户档案
     */
    public BusCustomerInfo selectBusCustomerInfoById(Long id);

    /**
     * 查询客户档案列表
     * 
     * @param busCustomerInfo 客户档案
     * @return 客户档案集合
     */
    public List<BusCustomerInfo> selectBusCustomerInfoList(BusCustomerInfoQO busCustomerInfo);

    /**
     * 新增客户档案
     * 
     * @param busCustomerInfoVO 客户档案
     * @return 结果
     */
    public int insertBusCustomerInfo(BusCustomerInfoVO busCustomerInfoVO);

    /**
     * 修改客户档案
     * 
     * @param busCustomerInfoVO 客户档案
     * @return 结果
     */
    public int updateBusCustomerInfo(BusCustomerInfoVO busCustomerInfoVO);

    /**
     * 批量删除客户档案
     * 
     * @param ids 需要删除的客户档案主键集合
     * @return 结果
     */
    public int deleteBusCustomerInfoByIds(Long[] ids);

    /**
     * 删除客户档案信息
     * 
     * @param id 客户档案主键
     * @return 结果
     */
    public int deleteBusCustomerInfoById(Long id);

    public void insertInfo();
}
