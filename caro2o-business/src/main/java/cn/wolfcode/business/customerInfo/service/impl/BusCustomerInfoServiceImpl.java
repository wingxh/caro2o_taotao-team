package cn.wolfcode.business.customerInfo.service.impl;

import java.util.Date;
import java.util.List;

import cn.wolfcode.business.appointment.domain.BusAppointment;
import cn.wolfcode.business.appointment.mapper.BusAppointmentMapper;
import cn.wolfcode.business.appointment.util.RegexUtils;
import cn.wolfcode.business.customerInfo.domain.vo.BusCustomerInfoQO;
import cn.wolfcode.business.customerInfo.domain.vo.BusCustomerInfoVO;
import cn.wolfcode.business.statement.domain.BusStatement;
import cn.wolfcode.business.statement.mapper.BusStatementMapper;
import cn.wolfcode.common.utils.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.wolfcode.business.customerInfo.mapper.BusCustomerInfoMapper;
import cn.wolfcode.business.customerInfo.domain.BusCustomerInfo;
import cn.wolfcode.business.customerInfo.service.IBusCustomerInfoService;
import org.springframework.util.Assert;

/**
 * 客户档案Service业务层处理
 * 
 * @author wolfcode
 * @date 2023-06-10
 */
@Service
public class BusCustomerInfoServiceImpl implements IBusCustomerInfoService 
{
    @Autowired
    private BusCustomerInfoMapper busCustomerInfoMapper;
    @Autowired
    private BusAppointmentMapper busAppointmentMapper;
    @Autowired
    private BusStatementMapper busStatementMapper;
    /**
     * 查询客户档案
     * 
     * @param id 客户档案主键
     * @return 客户档案
     */
    @Override
    public BusCustomerInfo selectBusCustomerInfoById(Long id)
    {
        return busCustomerInfoMapper.selectBusCustomerInfoById(id);
    }

    /**
     * 查询客户档案列表
     * 
     * @param busCustomerInfo 客户档案
     * @return 客户档案
     */
    @Override
    public List<BusCustomerInfo> selectBusCustomerInfoList(BusCustomerInfoQO busCustomerInfo)
    {
        return busCustomerInfoMapper.selectBusCustomerInfoList(busCustomerInfo);
    }

    /**
     * 新增客户档案
     * 
     * @param busCustomerInfoVO 客户档案
     * @return 结果
     */
    @Override
    public int insertBusCustomerInfo(BusCustomerInfoVO busCustomerInfoVO)
    {
        Assert.notNull(busCustomerInfoVO,"非法操作");
        Assert.notNull(busCustomerInfoVO.getCustomerName(),"客户名不能为空");
        Assert.state(RegexUtils.isPhoneLegal(busCustomerInfoVO.getCustomerPhone()),"电话格式有误");
        Assert.state(busCustomerInfoMapper.selectBusCustomerInfoByPhone(busCustomerInfoVO.getCustomerPhone()) == null,"账号已存在");
        Assert.notNull(busCustomerInfoVO.getGender(),"性别不能为空");
        BusCustomerInfo busCustomerInfo = new BusCustomerInfo();
        BeanUtils.copyProperties(busCustomerInfoVO,busCustomerInfo);
        busCustomerInfo.setClerk(SecurityUtils.getUsername());
        busCustomerInfo.setEnterTime(new Date());
        return busCustomerInfoMapper.insertBusCustomerInfo(busCustomerInfo);
    }

    /**
     * 修改客户档案
     * 
     * @param busCustomerInfoVO 客户档案
     * @return 结果
     */
    @Override
    public int updateBusCustomerInfo(BusCustomerInfoVO busCustomerInfoVO)
    {
        Assert.notNull(busCustomerInfoVO,"非法操作");
        Assert.notNull(busCustomerInfoVO.getCustomerName(),"客户名不能为空");

        Assert.state(RegexUtils.isPhoneLegal(busCustomerInfoVO.getCustomerPhone()),"电话格式有误");
        Assert.state(busCustomerInfoMapper.selectBusCustomerInfoByPhone(busCustomerInfoVO.getCustomerPhone()) == null,"账号已存在");
        Assert.notNull(busCustomerInfoVO.getGender(),"性别不能为空");

        BusCustomerInfo busCustomerInfo = selectBusCustomerInfoById(busCustomerInfoVO.getId());
        Assert.notNull(busCustomerInfo,"非法操作");

        BeanUtils.copyProperties(busCustomerInfoVO,busCustomerInfo);
        return busCustomerInfoMapper.updateBusCustomerInfo(busCustomerInfo);
    }

    /**
     * 批量删除客户档案
     * 
     * @param ids 需要删除的客户档案主键
     * @return 结果
     */
    @Override
    public int deleteBusCustomerInfoByIds(Long[] ids)
    {
        return busCustomerInfoMapper.deleteBusCustomerInfoByIds(ids);
    }

    /**
     * 删除客户档案信息
     * 
     * @param id 客户档案主键
     * @return 结果
     */
    @Override
    public int deleteBusCustomerInfoById(Long id)
    {
        return busCustomerInfoMapper.deleteBusCustomerInfoById(id);
    }

    @Override
    public void insertInfo(){
        List<BusAppointment> list = busAppointmentMapper.selectAllList();
        BusCustomerInfo busCustomerInfo = new BusCustomerInfo();
        for (BusAppointment busAppointment : list) {
            busCustomerInfo.setCustomerName(busAppointment.getCustomerName());
            busCustomerInfo.setCustomerPhone(busAppointment.getCustomerPhone());
            busCustomerInfo.setGender(0);
            busCustomerInfo.setClerk(SecurityUtils.getUsername());
            busCustomerInfo.setEnterTime(new Date());
            if(busCustomerInfoMapper.selectBusCustomerInfoByPhone(busCustomerInfo.getCustomerPhone()) == null){
                busCustomerInfoMapper.insertBusCustomerInfo(busCustomerInfo);
            }
        }
        List<BusStatement> statements = busStatementMapper.selectAllList();
        for (BusStatement busStatement : statements) {
            busCustomerInfo.setCustomerName(busStatement.getCustomerName());
            busCustomerInfo.setCustomerPhone(busStatement.getCustomerPhone());
            busCustomerInfo.setGender(0);
            busCustomerInfo.setClerk(SecurityUtils.getUsername());
            busCustomerInfo.setEnterTime(new Date());
            if(busCustomerInfoMapper.selectBusCustomerInfoByPhone(busCustomerInfo.getCustomerPhone()) == null){
                busCustomerInfoMapper.insertBusCustomerInfo(busCustomerInfo);
            }
        }
    }
}
