package cn.wolfcode.business.inventory.service.impl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.List;

import cn.wolfcode.business.inventory.domain.InventoryManagement;
import cn.wolfcode.business.inventory.domain.vo.ParticularsVO;
import cn.wolfcode.business.inventory.mapper.InventoryManagementMapper;
import cn.wolfcode.business.inventory.service.IInventoryManagementService;
import cn.wolfcode.business.warehouse.mapper.BusWarehouseInfoMapper;
import cn.wolfcode.store.storePutOut.mapper.BusInventoryTypeMapper;
import com.fasterxml.jackson.databind.util.ByteBufferBackedInputStream;
import io.jsonwebtoken.lang.Assert;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 * 库存管理Service业务层处理
 *
 * @author sakura
 * @date 2023-06-16
 */
@Service
@Transactional
public class InventoryManagementServiceImpl implements IInventoryManagementService
{
    @Autowired
    private InventoryManagementMapper inventoryManagementMapper;


    /**
     * 查询库存管理
     *
     * @param id 库存管理主键
     * @return 库存管理
     */
    @Override
    public InventoryManagement selectInventoryManagementById(Long id)
    {
        return inventoryManagementMapper.selectInventoryManagementById(id);
    }

    /**
     * 查询库存管理列表
     *
     * @param inventoryManagement 库存管理
     * @return 库存管理
     */
    @Override
    public List<InventoryManagement> selectInventoryManagementList(InventoryManagement inventoryManagement)
    {
        List<InventoryManagement> inventoryManagements = inventoryManagementMapper
                .selectInventoryManagementList(inventoryManagement);
        for (InventoryManagement management : inventoryManagements) {
            management.setPng( "data:image/png;base64," + Base64.getEncoder().encodeToString(management.getModel()));
        }
        return inventoryManagements;
    }

    /**
     * 新增库存管理
     *
     * @param inventoryManagement 库存管理
     * @return 结果
     */
    @Override
    public int insertInventoryManagement(InventoryManagement inventoryManagement,MultipartFile file) throws IOException {


        //各种判空
        Assert.notNull(file, "图片不能为空");

        String extension = FilenameUtils.getExtension(file.getOriginalFilename());
        //
        if(!("png".equals(extension) || "jpg".equals(extension) || "jpeg".equals(extension))){
            throw new RuntimeException("只能上传图片");
        }

        inventoryManagement.setModel(file.getBytes());
        Assert.notNull(inventoryManagement, "非法操作");
        Assert.notNull(inventoryManagement.getName(),"名称不能为空");
        Assert.notNull(inventoryManagement.getQuantity(), "数量不能为空");
        Assert.notNull(inventoryManagement.getClassification(),"分类不能为空");

        //长度判断
        Assert.state(inventoryManagement.getName().length() < 50,"名称不能超过50字");
        Assert.state(inventoryManagement.getBrand().length() < 50,"品牌不能超过50字");
        Assert.state(inventoryManagement.getDescription().length() < 200,"描述不能超过200字");

        if("null".equals(inventoryManagement.getBrand())){
            inventoryManagement.setBrand("未知");
        }
        if("null".equals(inventoryManagement.getDescription())){
            inventoryManagement.setDescription("无");
        }
        int i = inventoryManagementMapper.insertInventoryManagement(inventoryManagement);
        if(inventoryManagement.getWarehouseId() != null){
            inventoryManagementMapper.insertByWarehouse(inventoryManagement.getWarehouseId(),inventoryManagement.getId());
        }
        return i;
    }

    /**
     * 修改库存管理
     *
     * @param inventoryManagement 库存管理
     * @return 结果
     */
    @Override
    public int updateInventoryManagement(InventoryManagement inventoryManagement, MultipartFile file) throws IOException {
        if(file != null){
            inventoryManagement.setModel(file.getBytes());
        }
        Assert.notNull(inventoryManagement, "非法操作");
        Assert.notNull(inventoryManagement.getName(),"名称不能为空");
        Assert.notNull(inventoryManagement.getQuantity(), "数量不能为空");
        Assert.notNull(inventoryManagement.getClassification(),"分类不能为空");
        Assert.state(inventoryManagement.getName().length() < 50,"名称不能超过50字");
        Assert.state(inventoryManagement.getBrand().length() < 50,"品牌不能超过50字");
        Assert.state(inventoryManagement.getDescription().length() < 200,"描述不能超过200字");
        inventoryManagementMapper.updateByWarehouse(inventoryManagement.getWarehouseId(),inventoryManagement.getId());
        return inventoryManagementMapper.updateInventoryManagement(inventoryManagement);
    }

    /**
     * 批量删除库存管理
     *
     * @param ids 需要删除的库存管理主键
     * @return 结果
     */
    @Override
    public int deleteInventoryManagementByIds(Long[] ids)
    {

        Assert.notNull(ids, "非法操作");
        inventoryManagementMapper.deleteByWarehouse(ids);
        return inventoryManagementMapper.deleteInventoryManagementByIds(ids);
    }

    /**
     * 删除库存管理信息
     *
     * @param id 库存管理主键
     * @return 结果
     */
    @Override
    public int deleteInventoryManagementById(Long id)
    {
        Assert.notNull(id, "非法操作");
        return inventoryManagementMapper.deleteInventoryManagementById(id);
    }

    @Override
    public List<ParticularsVO> getParticulars(Long id) {
        Assert.notNull(id, "非法操作");
        List<ParticularsVO> particularsVOS = inventoryManagementMapper.queryByRepertoryId(id);
        return particularsVOS;
    }
}
