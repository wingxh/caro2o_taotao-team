package cn.wolfcode.business.inventory.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import cn.wolfcode.common.annotation.Excel;
import cn.wolfcode.common.core.domain.BaseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;

/**
 * 库存管理对象 inventory_management
 *
 * @author sakura
 * @date 2023-06-16
 */
public class InventoryManagement extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 物品 */
    @Excel(name = "物品")
    private byte[] model;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 品牌 */
    @Excel(name = "品牌")
    private String brand;

    /** 分类 */
    @Excel(name = "分类")
    private Long classification;

    /** 数量 */
    @Excel(name = "数量")
    private Long quantity;

    //仓库id
    private Integer warehouseId;

    /** 描述 */
    @Excel(name = "描述")
    private String description;

    private String png;

    public Integer getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Integer warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getPng() {
        return png;
    }

    public void setPng(String png) {
        this.png = png;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setModel(byte[] model)
    {
        this.model = model;
    }

    public byte[] getModel()
    {
        return model;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setBrand(String brand)
    {
        this.brand = brand;
    }

    public String getBrand()
    {
        return brand;
    }
    public void setClassification(Long classification)
    {
        this.classification = classification;
    }

    public Long getClassification()
    {
        return classification;
    }
    public void setQuantity(Long quantity)
    {
        this.quantity = quantity;
    }

    public Long getQuantity()
    {
        return quantity;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getDescription()
    {
        return description;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("model", getModel())
                .append("name", getName())
                .append("brand", getBrand())
                .append("classification", getClassification())
                .append("quantity", getQuantity())
                .append("description", getDescription())
                .toString();
    }
}
