package cn.wolfcode.business.statement.domain.vo;

import cn.wolfcode.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BusStatementVO {
    private Long id;

    private String customerName;

    private String customerPhone;

    private String licensePlate;

    private String carSeries;

    private Integer serviceType;

    private String info;

}
