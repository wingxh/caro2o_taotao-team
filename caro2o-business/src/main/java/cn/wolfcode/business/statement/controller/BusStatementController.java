package cn.wolfcode.business.statement.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import cn.wolfcode.business.statement.domain.vo.BusStatementQuery;
import cn.wolfcode.business.statement.domain.vo.BusStatementVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.wolfcode.common.annotation.Log;
import cn.wolfcode.common.core.controller.BaseController;
import cn.wolfcode.common.core.domain.AjaxResult;
import cn.wolfcode.common.enums.BusinessType;
import cn.wolfcode.business.statement.domain.BusStatement;
import cn.wolfcode.business.statement.service.IBusStatementService;
import cn.wolfcode.common.utils.poi.ExcelUtil;
import cn.wolfcode.common.core.page.TableDataInfo;

/**
 * 结算单Controller
 * 
 * @author wolfcode
 * @date 2023-05-31
 */
@RestController
@RequestMapping("/business/statement")
public class BusStatementController extends BaseController
{
    @Autowired
    private IBusStatementService busStatementService;

    /**
     * 查询结算单列表
     */
    @PreAuthorize("@ss.hasPermi('business:statement:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusStatementQuery busStatement)
    {
        startPage();
        List<BusStatement> list = busStatementService.selectBusStatementQOList(busStatement);
        return getDataTable(list);
    }

    /**
     * 获取结算单详细信息
     */
    @PreAuthorize("@ss.hasPermi('business:statement:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(busStatementService.selectBusStatementById(id));
    }

    /**
     * 新增结算单
     */
    @PreAuthorize("@ss.hasPermi('business:statement:add')")
    @PostMapping
    public AjaxResult add(@RequestBody BusStatementVO busStatementVO)
    {
        return toAjax(busStatementService.insertBusStatement(busStatementVO));
    }

    /**
     * 修改结算单
     */
    @PreAuthorize("@ss.hasPermi('business:statement:edit')")
    @PutMapping
    public AjaxResult edit(@RequestBody BusStatementVO busStatementVO)
    {
        return toAjax(busStatementService.updateBusStatement(busStatementVO));
    }
    @PostMapping("/synMsg/{id}")
    public AjaxResult synMsg(@PathVariable Long id)
    {
        return toAjax(busStatementService.synMsg(id));
    }

    /**
     * 删除结算单
     */
//    @PreAuthorize("@ss.hasPermi('business:statement:remove')")
//	@DeleteMapping("/{id}")
//    public AjaxResult remove(@PathVariable Long id)
//    {
//        return toAjax(busStatementService.deleteBusStatementById(id));
//    }
}
