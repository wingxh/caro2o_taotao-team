package cn.wolfcode.business.contract.domain.vo;

import cn.wolfcode.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.sun.mail.imap.protocol.INTERNALDATE;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@ToString
public class BusContractVO {

    private Long id;
    private String fileName;

    private String enterpriseName;

    private Integer fileSize;
    /**
     * 合同名称
     */
    private String contractName;

    /**
     * 合同编号
     */
    private String contractNumber;

    /**
     * 合同金额
     */
    private BigDecimal contractAmount;

    /**
     * 合同开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;

    /**
     * 合同结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;
}
