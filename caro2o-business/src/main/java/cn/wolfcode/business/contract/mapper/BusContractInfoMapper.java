package cn.wolfcode.business.contract.mapper;

import java.util.List;
import cn.wolfcode.business.contract.domain.BusContractInfo;
import org.apache.ibatis.annotations.Param;

/**
 * 合同管理Mapper接口
 * 
 * @author ruoyi
 * @date 2023-06-17
 */
public interface BusContractInfoMapper 
{
    /**
     * 查询合同管理
     * 
     * @param id 合同管理主键
     * @return 合同管理
     */
    public BusContractInfo selectBusContractInfoById(Long id);

    /**
     * 查询合同管理列表
     * 
     * @param busContractInfo 合同管理
     * @return 合同管理集合
     */
    public List<BusContractInfo> selectBusContractInfoList(BusContractInfo busContractInfo);

    /**
     * 新增合同管理
     * 
     * @param busContractInfo 合同管理
     * @return 结果
     */
    public int insertBusContractInfo(BusContractInfo busContractInfo);

    /**
     * 修改合同管理
     * 
     * @param busContractInfo 合同管理
     * @return 结果
     */
    public int updateBusContractInfo(BusContractInfo busContractInfo);

    /**
     * 删除合同管理
     * 
     * @param id 合同管理主键
     * @return 结果
     */
    public int deleteBusContractInfoById(Long id);

    /**
     * 批量删除合同管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusContractInfoByIds(Long[] ids);

    void updateCancelStatusById(@Param("id") Long id, @Param("cancelStatus") Integer cancelStatus);

    void updateStampStatusById(@Param("id") Long id, @Param("stampStatus") Integer stampStatus);

    void updateAuditStatusById(@Param("id") Long id, @Param("AuditStatus") Integer AuditStatus);
}
