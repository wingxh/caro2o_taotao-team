package cn.wolfcode.business.contract.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import cn.wolfcode.common.annotation.Excel;
import cn.wolfcode.common.core.domain.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 合同管理对象 bus_contract_info
 *
 * @author ruoyi
 * @date 2023-06-17
 */
public class BusContractInfo extends BaseEntity {
    private static final long serialVersionUID = 1L;
    //审核状态
    public static final Integer AUDIt_STATUS_REVIEW = 0;//审核中
    public static final Integer AUDIt_STATUS_SUCCESS = 1;//审核通过
    public static final Integer AUDIt_STATUS_FAILED = 2;//审核失败
    //盖章状态
    public static final Integer STAMP_STATUS_FALSE = 0;//未盖章
    public static final Integer STAMP_STATUS_TRUE = 1; //已盖章
    //作废状态
    public static final Integer CANCEL_STATUS_FALSE = 0;//未作废
    public static final Integer CANCEL_STATUS_TRUE = 1;//已作废

    /**
     *
     */
    private Long id;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    private String key;

    public Integer getFileSize() {
        return fileSize;
    }

    public void setFileSize(Integer fileSize) {
        this.fileSize = fileSize;
    }

    private Integer fileSize;
    /**
     * 所属企业客户
     */
    @Excel(name = "所属企业客户")
    private String enterpriseName;

    /**
     * 合同名称
     */
    @Excel(name = "合同名称")
    private String contractName;

    /**
     * 合同编号
     */
    @Excel(name = "合同编号")
    private String contractNumber;

    /**
     * 合同金额
     */
    @Excel(name = "合同金额")
    private BigDecimal contractAmount;

    /**
     * 合同开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "合同开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /**
     * 合同结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "合同结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /**
     * 合同电子附件
     */
    @Excel(name = "合同电子附件")
    private byte[] contractAnnex;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    //文件名称
    private String fileName;
    /**
     * 录入人
     */
    @Excel(name = "录入人")
    private String entryPerson;

    /**
     * 录入时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "录入时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date entryTime;

    /**
     * 审核状态
     */
    @Excel(name = "审核状态")
    private Integer status;

    /**
     * 是否盖章[0/未盖章][1/盖章]
     */
    @Excel(name = "是否盖章[0/未盖章][1/盖章]")
    private Integer isStamp;

    /**
     * 是否作废[0/未作废][1/作废]
     */
    @Excel(name = "是否作废[0/未作废][1/作废]")
    private Integer isCancel;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setEnterpriseName(String enterpriseName) {
        this.enterpriseName = enterpriseName;
    }

    public String getEnterpriseName() {
        return enterpriseName;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName;
    }

    public String getContractName() {
        return contractName;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractAmount(BigDecimal contractAmount) {
        this.contractAmount = contractAmount;
    }

    public BigDecimal getContractAmount() {
        return contractAmount;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setContractAnnex(byte[] contractAnnex) {
        this.contractAnnex = contractAnnex;
    }

    public byte[] getContractAnnex() {
        return contractAnnex;
    }

    public void setEntryPerson(String entryPerson) {
        this.entryPerson = entryPerson;
    }

    public String getEntryPerson() {
        return entryPerson;
    }

    public void setEntryTime(Date entryTime) {
        this.entryTime = entryTime;
    }

    public Date getEntryTime() {
        return entryTime;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }

    public void setIsStamp(Integer isStamp) {
        this.isStamp = isStamp;
    }

    public Integer getIsStamp() {
        return isStamp;
    }

    public void setIsCancel(Integer isCancel) {
        this.isCancel = isCancel;
    }

    public Integer getIsCancel() {
        return isCancel;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("enterpriseName", getEnterpriseName())
                .append("contractName", getContractName())
                .append("contractNumber", getContractNumber())
                .append("contractAmount", getContractAmount())
                .append("startTime", getStartTime())
                .append("endTime", getEndTime())
                .append("contractAnnex", getContractAnnex())
                .append("entryPerson", getEntryPerson())
                .append("entryTime", getEntryTime())
                .append("updateTime", getUpdateTime())
                .append("status", getStatus())
                .append("isStamp", getIsStamp())
                .append("isCancel", getIsCancel())
                .toString();
    }
}
