package cn.wolfcode.business.contract.controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletResponse;


import cn.wolfcode.business.contract.domain.vo.BusContractVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import cn.wolfcode.common.annotation.Log;
import cn.wolfcode.common.core.controller.BaseController;
import cn.wolfcode.common.core.domain.AjaxResult;
import cn.wolfcode.common.enums.BusinessType;
import cn.wolfcode.business.contract.domain.BusContractInfo;
import cn.wolfcode.business.contract.service.IBusContractInfoService;
import cn.wolfcode.common.utils.poi.ExcelUtil;
import cn.wolfcode.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 合同管理Controller
 * 
 * @author ruoyi
 * @date 2023-06-17
 */
@RestController
@RequestMapping("/business/contract")
public class BusContractInfoController extends BaseController
{
    @Autowired
    private IBusContractInfoService busContractInfoService;

    /**
     * 查询合同管理列表
     */
    @PreAuthorize("@ss.hasPermi('business:contract:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusContractInfo busContractInfo)
    {
        startPage();
        List<BusContractInfo> list = busContractInfoService.selectBusContractInfoList(busContractInfo);
        return getDataTable(list);
    }

    /**
     * 导出合同管理列表
     */
    @PreAuthorize("@ss.hasPermi('business:contract:export')")
    @Log(title = "合同管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusContractInfo busContractInfo)
    {
        List<BusContractInfo> list = busContractInfoService.selectBusContractInfoList(busContractInfo);
        ExcelUtil<BusContractInfo> util = new ExcelUtil<BusContractInfo>(BusContractInfo.class);
        util.exportExcel(response, list, "合同管理数据");
    }

    /**
     * 获取合同管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('business:contract:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(busContractInfoService.selectBusContractInfoById(id));
    }

    /**
     * 新增合同管理
     */
    @PreAuthorize("@ss.hasPermi('business:contract:add')")
    @Log(title = "合同管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(MultipartFile file, BusContractVO vo) throws IOException {
        busContractInfoService.insertBusContractInfo(file,vo);
        return AjaxResult.success();
    }


    /**
     * 修改合同管理
     */
    @PreAuthorize("@ss.hasPermi('business:contract:edit')")
    @Log(title = "合同管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(MultipartFile file, BusContractVO vo) throws IOException {
        busContractInfoService.updateBusContractInfo(file,vo);
        return AjaxResult.success();
    }

    /**
     * 作废合同管理
     */
    @PreAuthorize("@ss.hasPermi('business:contract:cancel')")
    @Log(title = "合同管理", businessType = BusinessType.DELETE)
	@PutMapping("/cancel/{id}")
    public AjaxResult remove(@PathVariable Long id)
    {
        busContractInfoService.deleteBusContractInfoById(id);
        return AjaxResult.success();
    }

    /**
     * 合同盖章管理
     */
    @PreAuthorize("@ss.hasPermi('business:contract:updateStamp')")
    @Log(title = "合同管理", businessType = BusinessType.DELETE)
    @PutMapping("/updateStamp/{id}")
    public AjaxResult updateStamp(@PathVariable Long id)
    {
        busContractInfoService.updateStamp(id);
        return AjaxResult.success();
    }

    //审核通过
    @PreAuthorize("@ss.hasPermi('business:contract:statusSuccess')")
    @Log(title = "合同管理", businessType = BusinessType.DELETE)
    @PutMapping("/statusSuccess/{id}")
    public AjaxResult statusSuccess(@PathVariable Long id)
    {
        busContractInfoService.updateStatusSuccess(id);
        return AjaxResult.success();
    }

    //审核通过
    @PreAuthorize("@ss.hasPermi('business:contract:statusFailed')")
    @Log(title = "合同管理", businessType = BusinessType.DELETE)
    @PutMapping("/statusFailed/{id}")
    public AjaxResult statusFailed(@PathVariable Long id)
    {
        busContractInfoService.updateStatusFailed(id);
        return AjaxResult.success();
    }
}
