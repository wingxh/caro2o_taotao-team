package cn.wolfcode.business.appointment.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import cn.wolfcode.business.appointment.domain.vo.BusAppointmentVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.wolfcode.common.core.controller.BaseController;
import cn.wolfcode.common.core.domain.AjaxResult;
import cn.wolfcode.business.appointment.domain.BusAppointment;
import cn.wolfcode.business.appointment.service.IBusAppointmentService;
import cn.wolfcode.common.core.page.TableDataInfo;

/**
 * 养修信息预约Controller
 * 
 * @author wolfcode
 * @date 2023-05-28
 */
@RestController
@RequestMapping("/business/appointment")
public class BusAppointmentController extends BaseController
{
    @Autowired
    private IBusAppointmentService busAppointmentService;

    /**
     * 查询养修信息预约列表
     */
    @PreAuthorize("@ss.hasPermi('business:appointment:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusAppointment busAppointment)
    {
        startPage();
        List<BusAppointment> list = busAppointmentService.selectBusAppointmentList(busAppointment);
        return getDataTable(list);
    }

    /**
     * 获取养修信息预约详细信息
     */
    @PreAuthorize("@ss.hasPermi('business:appointment:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(busAppointmentService.selectBusAppointmentById(id));
    }

    /**
     * 新增养修信息预约
     */
    @PreAuthorize("@ss.hasPermi('business:appointment:add')")
    @PostMapping
    public AjaxResult add(@RequestBody BusAppointmentVO busAppointmentVO)
    {
        return toAjax(busAppointmentService.insertBusAppointment(busAppointmentVO));
    }
    @PreAuthorize("@ss.hasPermi('business:appointment:edit')")
    @PutMapping
    public AjaxResult edit(@RequestBody BusAppointmentVO busAppointmentVO)
    {
        return toAjax(busAppointmentService.updateBusAppointment(busAppointmentVO));
    }



    /**
     * 删除养修信息预约
     */
    @PreAuthorize("@ss.hasPermi('business:appointment:remove')")
	@DeleteMapping("/{id}")
    public AjaxResult remove(@PathVariable Long id)
    {
        return toAjax(busAppointmentService.deleteBusAppointmentById(id));
    }

    /**
     * 取消养修信息预约
     */
    @PreAuthorize("@ss.hasPermi('business:appointment:cancelStatus')")
    @PutMapping("/cancelStatus/{id}")
    public AjaxResult cancelStatus(@PathVariable Long id)
    {
        return toAjax(busAppointmentService.cancelStatus(id));
    }


    @PostMapping("/synMsg/{id}")
    public AjaxResult synMsg(@PathVariable Long id)
    {
        return toAjax(busAppointmentService.synMsg(id));
    }

    /**
     * 到点
     */
    @PreAuthorize("@ss.hasPermi('business:appointment:arriveShop')")
    @PutMapping("/arriveStop/{id}")
    public AjaxResult changeArriveShopStatus(@PathVariable Long id)
    {
        return toAjax(busAppointmentService.changeArriveShopStatus(id));
    }

    /**
     * 获取预约的结算表
     */
    @PreAuthorize("@ss.hasPermi('business:appointment:settle')")
    @PostMapping("/settle/{id}")
    public AjaxResult appointmentSettle(@PathVariable Long id)
    {
        return AjaxResult.success(busAppointmentService.getBusStatementIdByAppointmentSettle(id));
    }


}
