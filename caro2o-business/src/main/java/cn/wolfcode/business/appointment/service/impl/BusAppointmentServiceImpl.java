package cn.wolfcode.business.appointment.service.impl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import cn.wolfcode.business.appointment.domain.vo.BusAppointmentVO;
import cn.wolfcode.business.appointment.util.RegexUtils;
import cn.wolfcode.business.appointment.util.VehiclePlateNoUtil;
import cn.wolfcode.business.customerInfo.domain.BusCustomerInfo;
import cn.wolfcode.business.customerInfo.mapper.BusCustomerInfoMapper;
import cn.wolfcode.business.statement.domain.BusStatement;
import cn.wolfcode.business.statement.mapper.BusStatementMapper;
import cn.wolfcode.common.utils.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import cn.wolfcode.business.appointment.mapper.BusAppointmentMapper;
import cn.wolfcode.business.appointment.domain.BusAppointment;
import cn.wolfcode.business.appointment.service.IBusAppointmentService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * 养修信息预约Service业务层处理
 * 
 * @author wolfcode
 * @date 2023-05-28
 */
@Service
@Transactional
public class BusAppointmentServiceImpl implements IBusAppointmentService 
{
    @Autowired
    private BusAppointmentMapper busAppointmentMapper;
    @Autowired
    private BusStatementMapper busStatementMapper;
    @Autowired
    private BusCustomerInfoMapper busCustomerInfoMapper;
    /**
     * 查询养修信息预约
     * 
     * @param id 养修信息预约主键
     * @return 养修信息预约
     */
    @Override
    public BusAppointment selectBusAppointmentById(Long id)
    {
        return busAppointmentMapper.selectBusAppointmentById(id);
    }

    /**
     * 查询养修信息预约列表
     * 
     * @param busAppointment 养修信息预约
     * @return 养修信息预约
     */
    @Override
    public List<BusAppointment> selectBusAppointmentList(BusAppointment busAppointment)
    {

        return busAppointmentMapper.selectBusAppointmentList(busAppointment);
    }

    /**
     * 新增养修信息预约
     * 
     * @param busAppointmentVO 养修信息预约
     * @return 结果
     */
    @Override
    public int insertBusAppointment(BusAppointmentVO busAppointmentVO)
    {
        //1 判断VO非空
        if (busAppointmentVO == null) {
            throw new RuntimeException("非法操作");
        }
        //2 做手机号码校验
        if(!RegexUtils.isPhoneLegal(busAppointmentVO.getCustomerPhone())){
            throw new RuntimeException("非法操作");
        }
        //3 预约时间校验
        if(new Date().after(busAppointmentVO.getAppointmentTime())){
            throw new RuntimeException("预约时间不合理");
        }
        //4 车牌号码校验
        if(VehiclePlateNoUtil.getVehiclePlateNo(busAppointmentVO.getLicensePlate()) == null){
            throw new RuntimeException("车牌号码不合法");
        }
        //5 把VO的数据封装在BusAppointment中
        BusAppointment busAppointment = new BusAppointment();
        BeanUtils.copyProperties(busAppointmentVO,busAppointment);
        //6 设置创建时间和状态
        busAppointment.setCreateTime(new Date());
        busAppointment.setStatus(BusAppointment.STATUS_APPOINTMENT);
        //7 保存到数据库中
        return busAppointmentMapper.insertBusAppointment(busAppointment);
    }

    /**
     * 修改养修信息预约
     * 
     * @param busAppointmentVO 养修信息预约
     * @return 结果
     */
    @Override
    public int updateBusAppointment(BusAppointmentVO busAppointmentVO)
    {
        //1 判断VO非空
        if (busAppointmentVO == null) {
            throw new RuntimeException("非法操作");
        }
        //2 做手机号码校验
        if(!RegexUtils.isPhoneLegal(busAppointmentVO.getCustomerPhone())){
            throw new RuntimeException("非法操作");
        }
        //3 预约时间校验
        if(new Date().after(busAppointmentVO.getAppointmentTime())){
            throw new RuntimeException("预约时间不合理");
        }
        //4 车牌号码校验
        if(VehiclePlateNoUtil.getVehiclePlateNo(busAppointmentVO.getLicensePlate()) == null){
            throw new RuntimeException("车牌号码不合法");
        }
        //5 根据id查询数据
        BusAppointment busAppointment = busAppointmentMapper.selectBusAppointmentById(busAppointmentVO.getId());
        if(busAppointment == null){
            throw new RuntimeException("非法操作");
        }
        //6 判断状态是否为预约中状态
        if(!BusAppointment.STATUS_APPOINTMENT.equals(busAppointment.getStatus())){
            throw new RuntimeException("状态必须为预约中");
        }
        //7 把Vo的数据封装到BusAppointment对象中
        BeanUtils.copyProperties(busAppointmentVO,busAppointment);
        return busAppointmentMapper.updateBusAppointment(busAppointment);
    }

    /**
     * 批量删除养修信息预约
     * 
     * @param ids 需要删除的养修信息预约主键
     * @return 结果
     */
    @Override
    public int deleteBusAppointmentByIds(Long[] ids)
    {
        return busAppointmentMapper.deleteBusAppointmentByIds(ids);
    }

    /**
     * 删除养修信息预约信息
     * 
     * @param id 养修信息预约主键
     * @return 结果
     */
    @Override
    public int deleteBusAppointmentById(Long id)
    {
        if(id == null){
            throw new RuntimeException("非法操作");
        }
        BusAppointment busAppointment = busAppointmentMapper.selectBusAppointmentById(id);
        if(busAppointment == null){
            throw new RuntimeException("非法操作");
        }

        if(!(BusAppointment.STATUS_APPOINTMENT.equals(busAppointment.getStatus()) ||
                BusAppointment.STATUS_CANCEL.equals(busAppointment.getStatus()))){
            throw new RuntimeException("状态必须为预约中状态或取消状态");
        }
        return busAppointmentMapper.deleteBusAppointmentById(id);
    }

    @Override
    public int changeArriveShopStatus(Long id) {
        if(id == null){
            throw new RuntimeException("非法操作");
        }
        BusAppointment busAppointment = busAppointmentMapper.selectBusAppointmentById(id);
        if(busAppointment == null){
            throw new RuntimeException("非法操作");
        }

        if(!BusAppointment.STATUS_APPOINTMENT.equals(busAppointment.getStatus())){
            throw new RuntimeException("状态必须为预约中状态");
        }
        return busAppointmentMapper.changeArriveShopStatus(id,BusAppointment.STATUS_ARRIVAL);
    }

    @Override
    public int cancelStatus(Long id) {
        if(id == null){
            throw new RuntimeException("非法操作");
        }
        BusAppointment busAppointment = busAppointmentMapper.selectBusAppointmentById(id);
        if(busAppointment == null){
            throw new RuntimeException("非法操作");
        }

        if(!BusAppointment.STATUS_APPOINTMENT.equals(busAppointment.getStatus())){
            throw new RuntimeException("状态必须为预约中状态");
        }
        return busAppointmentMapper.changeStatus(id,BusAppointment.STATUS_CANCEL);
    }

    @Override
    public Long getBusStatementIdByAppointmentSettle(Long id) {
        //参数校验
        Assert.notNull(id,"非法操作");
        //1、通过预约id查预约单数据
        BusAppointment busAppointment = busAppointmentMapper.selectBusAppointmentById(id);
        //2、判断该id是否已经创建了明细表
        BusStatement busStatement = busStatementMapper.selectBusStatementByAppointmentId(id);
        //3、如果没有创建表则将预约单的数据封装到结算单中，
        if(busStatement == null){
            busStatement = new BusStatement();
            busStatement.setCustomerName(busAppointment.getCustomerName());
            busStatement.setCustomerPhone(busAppointment.getCustomerPhone());
            busStatement.setActualArrivalTime(busAppointment.getActualArrivalTime());
            busStatement.setLicensePlate(busAppointment.getLicensePlate());
            busStatement.setCarSeries(busAppointment.getCarSeries());
            busStatement.setServiceType(busAppointment.getServiceType());
            busStatement.setAppointmentId(busAppointment.getId());
            busStatement.setStatus(BusStatement.STATUS_CONSUME);
            busStatementMapper.insertBusStatement(busStatement);

            busAppointment.setStatus(BusAppointment.STATUS_SETTLE);
            busAppointmentMapper.updateBusAppointment(busAppointment);
        }
        //4、修改预约单中的状态为结算中
        //5、返回结算单的id
        return busStatement.getId();
    }

    @Override
    @Scheduled(cron = "0 0 */1 * * ?")
    public void overtimeStatus() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH ,-1);
        Date selectTime = calendar.getTime();
        List<BusAppointment> list =  busAppointmentMapper.selectAll(selectTime);
        for (BusAppointment busAppointment : list) {
            System.out.println(busAppointment);
            if(busAppointment.getStatus() == 0){
                Date appointmentTime = busAppointment.getAppointmentTime();
                calendar.setTime(appointmentTime);
                calendar.add(Calendar.HOUR_OF_DAY , 1);
                Date time = calendar.getTime();
                if(time.before(new Date())){
                    System.out.println(busAppointment.getCustomerName() + "你快要超时了");
                }
                calendar.add(Calendar.HOUR_OF_DAY , 3);
                Date time1 = calendar.getTime();
                if(time1.before(new Date())){
                    busAppointmentMapper.changeStatus(busAppointment.getId(),BusAppointment.STATUS_OVERTIME);
                }
            }
        }
    }

    @Override
    public int synMsg(Long id) {
        BusAppointment busAppointment = busAppointmentMapper.selectBusAppointmentById(id);

        BusCustomerInfo busCustomerInfo = new BusCustomerInfo();
        busCustomerInfo.setCustomerName(busAppointment.getCustomerName());
        busCustomerInfo.setCustomerPhone(busAppointment.getCustomerPhone());
        busCustomerInfo.setGender(0);
        busCustomerInfo.setClerk(SecurityUtils.getUsername());
        busCustomerInfo.setEnterTime(new Date());
        if(busCustomerInfoMapper.selectBusCustomerInfoByPhone(busCustomerInfo.getCustomerPhone()) == null){
            busCustomerInfoMapper.insertBusCustomerInfo(busCustomerInfo);
        }
        return 1;
    }
}
