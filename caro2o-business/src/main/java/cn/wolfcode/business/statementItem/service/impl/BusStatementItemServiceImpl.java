package cn.wolfcode.business.statementItem.service.impl;

import java.math.BigDecimal;
import java.security.Security;
import java.util.Date;
import java.util.List;

import cn.wolfcode.business.appointment.domain.BusAppointment;
import cn.wolfcode.business.appointment.mapper.BusAppointmentMapper;
import cn.wolfcode.business.statement.domain.BusStatement;
import cn.wolfcode.business.statement.mapper.BusStatementMapper;
import cn.wolfcode.business.statementItem.domain.vo.BusStatementItemVO;
import cn.wolfcode.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.wolfcode.business.statementItem.mapper.BusStatementItemMapper;
import cn.wolfcode.business.statementItem.domain.BusStatementItem;
import cn.wolfcode.business.statementItem.service.IBusStatementItemService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * 结算单明细Service业务层处理
 * 
 * @author wolfcode
 * @date 2023-05-31
 */
@Service
@Transactional
public class BusStatementItemServiceImpl implements IBusStatementItemService 
{
    @Autowired
    private BusStatementItemMapper busStatementItemMapper;
    @Autowired
    private BusStatementMapper busStatementMapper;
    @Autowired
    private BusAppointmentMapper busAppointmentMapper;

    /**
     * 查询结算单明细
     * 
     * @param id 结算单明细主键
     * @return 结算单明细
     */
    @Override
    public BusStatementItem selectBusStatementItemById(Long id)
    {
        return busStatementItemMapper.selectBusStatementItemById(id);
    }

    /**
     * 查询结算单明细列表
     * 
     * @param busStatementItem 结算单明细
     * @return 结算单明细
     */
    @Override
    public List<BusStatementItem> selectBusStatementItemList(BusStatementItem busStatementItem)
    {
        return busStatementItemMapper.selectBusStatementItemList(busStatementItem);
    }

    /**
     * 新增结算单明细
     * 
     * @param busStatementItemVO 结算单明细
     * @return 结果
     */
    @Override
    public int insertBusStatementItem(BusStatementItemVO busStatementItemVO)
    {
        //1、参数校验
        Assert.notNull(busStatementItemVO,"非法操作");
        Assert.notNull(busStatementItemVO.getStatementId(),"非法操作");
        //2、根据id查结算单
        BusStatement busStatement = busStatementMapper.selectBusStatementById(busStatementItemVO.getStatementId());
        Assert.notNull(busStatement,"非法操作");
        Assert.state(BusStatement.STATUS_CONSUME.equals(busStatement.getStatus()),
                "结算单明细保存必须是消费中状态");
        //3、添加结算单中的总金额，总数量
        List<BusStatementItem> busStatementItems = busStatementItemVO.getBusStatementItems();
        //4、清空关系表中原来的数据,重新添加关系表数据
        busStatementItemMapper.deleteByBusStatementId(busStatementItemVO.getStatementId());
        busStatementItemMapper.addBusStatementItems(busStatementItems);
        //5、修改结算单中的数据
        BigDecimal totalAmount = BigDecimal.ZERO;
        BigDecimal totalQuantity = BigDecimal.ZERO;
        for (BusStatementItem busStatementItem : busStatementItems) {
            totalQuantity = totalQuantity.add(BigDecimal.valueOf(busStatementItem.getItemQuantity()));
            totalAmount = totalAmount.add(busStatementItem.getItemPrice()
                    .multiply(BigDecimal.valueOf(busStatementItem.getItemQuantity())));
        }
        busStatement.setTotalAmount(totalAmount);
        busStatement.setTotalQuantity(totalQuantity);
        Assert.state(totalAmount.compareTo(busStatementItemVO.getDiscountPrice()) >= 0,
                "优惠价格不能大于总金额");
        busStatement.setDiscountAmount(busStatementItemVO.getDiscountPrice());
        return busStatementMapper.updateBusStatement(busStatement);
    }

    /**
     * 修改结算单明细
     * 
     * @param busStatementItem 结算单明细
     * @return 结果
     */
    @Override
    public int updateBusStatementItem(BusStatementItem busStatementItem)
    {
        return busStatementItemMapper.updateBusStatementItem(busStatementItem);
    }

    /**
     * 批量删除结算单明细
     * 
     * @param ids 需要删除的结算单明细主键
     * @return 结果
     */
    @Override
    public int deleteBusStatementItemByIds(Long[] ids)
    {
        return busStatementItemMapper.deleteBusStatementItemByIds(ids);
    }

    /**
     * 删除结算单明细信息
     * 
     * @param id 结算单明细主键
     * @return 结果
     */
    @Override
    public int deleteBusStatementItemById(Long id)
    {
        return busStatementItemMapper.deleteBusStatementItemById(id);
    }

    @Override
    public int pay(Long id) {
        Assert.notNull(id,"非法操作");
        BusStatement busStatement = busStatementMapper.selectBusStatementById(id);
        Assert.notNull(busStatement,"非法操作");
        Assert.state(BusStatement.STATUS_CONSUME.equals(busStatement.getStatus()),
                "用户状态必须为消费中");

        busStatement.setStatus(BusStatement.STATUS_PAID);
        busStatement.setPayTime(new Date());
        busStatement.setPayeeId(id);

        if(busStatement.getAppointmentId() != null){
            busAppointmentMapper.changeStatus(busStatement.getAppointmentId(),
                    BusAppointment.STATUS_PAYED);
        }
        return busStatementMapper.updateBusStatement(busStatement);
    }
}
