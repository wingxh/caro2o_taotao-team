package cn.wolfcode.business.statementItem.domain.vo;

import cn.wolfcode.business.statementItem.domain.BusStatementItem;
import lombok.*;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BusStatementItemVO {
    private Long statementId;
    private BigDecimal discountPrice;
    private List<BusStatementItem> busStatementItems;

}
