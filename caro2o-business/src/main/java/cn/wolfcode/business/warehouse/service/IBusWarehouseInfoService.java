package cn.wolfcode.business.warehouse.service;

import java.util.List;
import cn.wolfcode.business.warehouse.domain.BusWarehouseInfo;

/**
 * 仓库信息Service接口
 * 
 * @author ruoyi
 * @date 2023-06-16
 */
public interface IBusWarehouseInfoService 
{
    /**
     * 查询仓库信息
     * 
     * @param id 仓库信息主键
     * @return 仓库信息
     */
    public BusWarehouseInfo selectBusWarehouseInfoById(Long id);

    /**
     * 查询仓库信息列表
     * 
     * @param busWarehouseInfo 仓库信息
     * @return 仓库信息集合
     */
    public List<BusWarehouseInfo> selectBusWarehouseInfoList(BusWarehouseInfo busWarehouseInfo);

    /**
     * 新增仓库信息
     * 
     * @param busWarehouseInfo 仓库信息
     * @return 结果
     */
    public int insertBusWarehouseInfo(BusWarehouseInfo busWarehouseInfo);

    /**
     * 修改仓库信息
     * 
     * @param busWarehouseInfo 仓库信息
     * @return 结果
     */
    public int updateBusWarehouseInfo(BusWarehouseInfo busWarehouseInfo);

    /**
     * 批量删除仓库信息
     * 
     * @param ids 需要删除的仓库信息主键集合
     * @return 结果
     */
    public int deleteBusWarehouseInfoByIds(Long[] ids);

    /**
     * 删除仓库信息信息
     * 
     * @param id 仓库信息主键
     * @return 结果
     */
    public int deleteBusWarehouseInfoById(Long id);
}
