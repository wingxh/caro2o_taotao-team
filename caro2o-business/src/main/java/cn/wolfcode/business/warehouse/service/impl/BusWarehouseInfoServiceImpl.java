package cn.wolfcode.business.warehouse.service.impl;

import java.util.List;

import cn.wolfcode.store.storePutOut.domain.BusInventoryType;
import cn.wolfcode.store.storePutOut.mapper.BusInventoryTypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.wolfcode.business.warehouse.mapper.BusWarehouseInfoMapper;
import cn.wolfcode.business.warehouse.domain.BusWarehouseInfo;
import cn.wolfcode.business.warehouse.service.IBusWarehouseInfoService;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

/**
 * 仓库信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-06-16
 */
@Service
public class BusWarehouseInfoServiceImpl implements IBusWarehouseInfoService 
{
    @Autowired
    private BusWarehouseInfoMapper busWarehouseInfoMapper;

    /**
     * 查询仓库信息
     * 
     * @param id 仓库信息主键
     * @return 仓库信息
     */
    @Override
    public BusWarehouseInfo selectBusWarehouseInfoById(Long id)
    {
        return busWarehouseInfoMapper.selectBusWarehouseInfoById(id);
    }

    /**
     * 查询仓库信息列表
     * 
     * @param busWarehouseInfo 仓库信息
     * @return 仓库信息
     */
    @Override
    public List<BusWarehouseInfo> selectBusWarehouseInfoList(BusWarehouseInfo busWarehouseInfo)
    {
        return busWarehouseInfoMapper.selectBusWarehouseInfoList(busWarehouseInfo);
    }

    /**
     * 新增仓库信息
     * 
     * @param busWarehouseInfo 仓库信息
     * @return 结果
     */
    @Override
    public int insertBusWarehouseInfo(BusWarehouseInfo busWarehouseInfo)
    {
        return busWarehouseInfoMapper.insertBusWarehouseInfo(busWarehouseInfo);
    }

    /**
     * 修改仓库信息
     * 
     * @param busWarehouseInfo 仓库信息
     * @return 结果
     */
    @Override
    public int updateBusWarehouseInfo(BusWarehouseInfo busWarehouseInfo)
    {
        Assert.notNull(busWarehouseInfo,"参数异常");
            return busWarehouseInfoMapper.updateBusWarehouseInfo(busWarehouseInfo);
     }

    /**
     * 批量删除仓库信息
     * 
     * @param ids 需要删除的仓库信息主键
     * @return 结果
     */
    @Override
    public int deleteBusWarehouseInfoByIds(Long[] ids)
    {
        return busWarehouseInfoMapper.deleteBusWarehouseInfoByIds(ids);
    }

    /**
     * 删除仓库信息信息
     * 
     * @param id 仓库信息主键
     * @return 结果
     */
    @Override
    public int deleteBusWarehouseInfoById(Long id)
    {
        int count =busWarehouseInfoMapper.selectRelationbyhouseId(id);
        Assert.state(count==0,"仓库中有物品,无法删除此仓库");
        return busWarehouseInfoMapper.deleteBusWarehouseInfoById(id);
    }
}
