package cn.wolfcode.business.warehouse.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.wolfcode.common.annotation.Log;
import cn.wolfcode.common.core.controller.BaseController;
import cn.wolfcode.common.core.domain.AjaxResult;
import cn.wolfcode.common.enums.BusinessType;
import cn.wolfcode.business.warehouse.domain.BusWarehouseInfo;
import cn.wolfcode.business.warehouse.service.IBusWarehouseInfoService;
import cn.wolfcode.common.utils.poi.ExcelUtil;
import cn.wolfcode.common.core.page.TableDataInfo;

/**
 * 仓库信息Controller
 * 
 * @author ruoyi
 * @date 2023-06-16
 */
@RestController
@RequestMapping("/business/warehouse")
public class BusWarehouseInfoController extends BaseController
{
    @Autowired
    private IBusWarehouseInfoService busWarehouseInfoService;

    /**
     * 查询仓库信息列表
     */
    @PreAuthorize("@ss.hasPermi('business:warehouse:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusWarehouseInfo busWarehouseInfo)
    {
        startPage();
        List<BusWarehouseInfo> list = busWarehouseInfoService.selectBusWarehouseInfoList(busWarehouseInfo);
        return getDataTable(list);
    }

    /**
     * 导出仓库信息列表
     */
    @PreAuthorize("@ss.hasPermi('business:warehouse:export')")
    @Log(title = "仓库信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusWarehouseInfo busWarehouseInfo)
    {
        List<BusWarehouseInfo> list = busWarehouseInfoService.selectBusWarehouseInfoList(busWarehouseInfo);
        ExcelUtil<BusWarehouseInfo> util = new ExcelUtil<BusWarehouseInfo>(BusWarehouseInfo.class);
        util.exportExcel(response, list, "仓库信息数据");
    }

    /**
     * 获取仓库信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('business:warehouse:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(busWarehouseInfoService.selectBusWarehouseInfoById(id));
    }

    /**
     * 新增仓库信息
     */
    @PreAuthorize("@ss.hasPermi('business:warehouse:add')")
    @Log(title = "仓库信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusWarehouseInfo busWarehouseInfo)
    {
        return toAjax(busWarehouseInfoService.insertBusWarehouseInfo(busWarehouseInfo));
    }

    /**
     * 修改仓库信息
     */
    @PreAuthorize("@ss.hasPermi('business:warehouse:edit')")
    @Log(title = "仓库信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusWarehouseInfo busWarehouseInfo)
    {
        return toAjax(busWarehouseInfoService.updateBusWarehouseInfo(busWarehouseInfo));
    }

    /**
     * 删除仓库信息
     */
    @PreAuthorize("@ss.hasPermi('business:warehouse:remove')")
    @Log(title = "仓库信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{id}")
    public AjaxResult remove(@PathVariable Long id)
    {
        return toAjax(busWarehouseInfoService.deleteBusWarehouseInfoById(id));
    }
}
