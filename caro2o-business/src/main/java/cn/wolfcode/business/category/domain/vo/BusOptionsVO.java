package cn.wolfcode.business.category.domain.vo;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BusOptionsVO {
    private Long id;
    private String name;
    private List<BusOptionsVO> children = null;
}
