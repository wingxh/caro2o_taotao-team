package cn.wolfcode.business.category.domain;

import cn.wolfcode.business.category.domain.vo.BusOptionsVO;
import cn.wolfcode.business.category.mapper.BusCategoryMapper;
import lombok.*;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import cn.wolfcode.common.annotation.Excel;
import cn.wolfcode.common.core.domain.BaseEntity;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.LongFunction;

/**
 * 分类管理对象 bus_category
 *
 * @author Mbester
 * @date 2023-06-16
 */
@Getter
@Setter
@ToString
public class BusCategory extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 分类id
     */
    private Long id;

    /**
     * 上级分类的id
     */
    private Long parentId;

    /**
     * 上级分类名称
     */
    private String parentName;

    /**
     * 分类名称
     */
    private String name;

    /**
     * 分类描述
     */
    private String description;

    /**
     * 上级路径
     */
    private String path;

    /**
     * 子分类集合
     */
    private List<BusCategory> children = null;

    public BusCategory() {
    }

    public BusCategory(Long id, String name, List<BusCategory> children) {
        this.id = id;
        this.name = name;
        this.children = children;
    }
}
