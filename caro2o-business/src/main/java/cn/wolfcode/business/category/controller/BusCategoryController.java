package cn.wolfcode.business.category.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import cn.wolfcode.business.category.domain.vo.BusCategoryParentVO;
import cn.wolfcode.business.category.domain.vo.BusOptionsVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.wolfcode.common.annotation.Log;
import cn.wolfcode.common.core.controller.BaseController;
import cn.wolfcode.common.core.domain.AjaxResult;
import cn.wolfcode.common.enums.BusinessType;
import cn.wolfcode.business.category.domain.BusCategory;
import cn.wolfcode.business.category.service.IBusCategoryService;
import cn.wolfcode.common.utils.poi.ExcelUtil;
import cn.wolfcode.common.core.page.TableDataInfo;

/**
 * 分类管理Controller
 * 
 * @author Mbester
 * @date 2023-06-16
 */
@RestController
@RequestMapping("/category/category")
public class BusCategoryController extends BaseController
{
    @Autowired
    private IBusCategoryService busCategoryService;

    /**
     * 查询分类管理列表
     */
    @PreAuthorize("@ss.hasPermi('category:category:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusCategory busCategory)
    {
        startPage();
        List<BusCategory> list = busCategoryService.selectBusCategoryList(busCategory);
        return getDataTable(list);
    }

    /**
     * 查询分类管理列表
     */
    @PreAuthorize("@ss.hasPermi('category:category:getParentNames')")
    @GetMapping("/getParentNames")
    public List<BusCategoryParentVO> getParentNames()
    {
        List<BusCategoryParentVO> parentNames = busCategoryService.getParentNames();
        return parentNames;
    }

    /**
     * 查询分类管理列表
     */
    @PreAuthorize("@ss.hasPermi('category:category:getOptions')")
    @GetMapping("/getOptions")
    public List<BusCategory> getOptions()
    {
        List<BusCategory> options = busCategoryService.getOptions();
        return options;
    }

    /**
     * 导出分类管理列表
     */
    @PreAuthorize("@ss.hasPermi('category:category:export')")
    @Log(title = "分类管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusCategory busCategory)
    {
        List<BusCategory> list = busCategoryService.selectBusCategoryList(busCategory);
        ExcelUtil<BusCategory> util = new ExcelUtil<BusCategory>(BusCategory.class);
        util.exportExcel(response, list, "分类管理数据");
    }

    /**
     * 获取分类管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('category:category:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(busCategoryService.selectBusCategoryById(id));
    }

    /**
     * 新增分类管理
     */
    @PreAuthorize("@ss.hasPermi('category:category:add')")
    @Log(title = "分类管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusCategory busCategory)
    {
        busCategoryService.insertBusCategory(busCategory);
        return AjaxResult.success();
    }

    /**
     * 修改分类管理
     */
    @PreAuthorize("@ss.hasPermi('category:category:edit')")
    @Log(title = "分类管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusCategory busCategory)
    {
        return toAjax(busCategoryService.updateBusCategory(busCategory));
    }

    /**
     * 删除分类管理
     */
    @PreAuthorize("@ss.hasPermi('category:category:remove')")
    @Log(title = "分类管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(busCategoryService.deleteBusCategoryByIds(ids));
    }
}
