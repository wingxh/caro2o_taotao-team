package cn.wolfcode.business.category.domain.vo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FormOptionsIdVo {
    private Long sourceid;//被迁移的物品
    private Long targetid;//迁移的目标物品
}
