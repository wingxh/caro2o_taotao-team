package cn.wolfcode.store.storePutOut.vo;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import cn.wolfcode.common.annotation.Excel;
import cn.wolfcode.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 entry_and_exit_details
 * 
 * @author ruoyi
 * @date 2023-06-16
 */
public class EntryAndExitDetails extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 类型出库入库类型 */
    @Excel(name = "类型出库入库类型")
    private Long type;

    /** 操作时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "操作时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date operationtime;

    /** 物品 */
    @Excel(name = "物品")
    private String goods;

    /** 品牌 */
    @Excel(name = "品牌")
    private String brand;

    /** 仓库 */
    @Excel(name = "仓库")
    private Long warehouse;

    /** 数量 */
    @Excel(name = "数量")
    private Long quantity;

    /** 单价 */
    @Excel(name = "单价")
    private BigDecimal unitprice;


    @Excel(name = "物品id")
    private Long goodsId;
    @Excel(name = "出入库管理id")
    private Long inventoryid;

    public Long getInventoryid() {
        return inventoryid;
    }

    public void setInventoryid(Long inventoryid) {
        this.inventoryid = inventoryid;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setOperationtime(Date operationtime) 
    {
        this.operationtime = operationtime;
    }

    public Date getOperationtime() 
    {
        return operationtime;
    }
    public void setGoods(String goods) 
    {
        this.goods = goods;
    }

    public String getGoods() 
    {
        return goods;
    }
    public void setBrand(String brand) 
    {
        this.brand = brand;
    }

    public String getBrand() 
    {
        return brand;
    }
    public void setWarehouse(Long warehouse)
    {
        this.warehouse = warehouse;
    }

    public Long getWarehouse()
    {
        return warehouse;
    }
    public void setQuantity(Long quantity) 
    {
        this.quantity = quantity;
    }

    public Long getQuantity() 
    {
        return quantity;
    }
    public void setUnitprice(BigDecimal unitprice) 
    {
        this.unitprice = unitprice;
    }

    public BigDecimal getUnitprice() 
    {
        return unitprice;
    }
    public void setGoodsId(Long goodsId) 
    {
        this.goodsId = goodsId;
    }

    public Long getGoodsId() 
    {
        return goodsId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("type", getType())
            .append("operationtime", getOperationtime())
            .append("goods", getGoods())
            .append("brand", getBrand())
            .append("warehouse", getWarehouse())
            .append("quantity", getQuantity())
            .append("unitprice", getUnitprice())
            .append("goodsId", getGoodsId())
            .toString();
    }
}
