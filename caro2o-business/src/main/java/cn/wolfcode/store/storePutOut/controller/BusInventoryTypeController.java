package cn.wolfcode.store.storePutOut.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import cn.wolfcode.business.warehouse.domain.BusWarehouseInfo;
import cn.wolfcode.store.storePutOut.vo.StoreItemVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.wolfcode.common.annotation.Log;
import cn.wolfcode.common.core.controller.BaseController;
import cn.wolfcode.common.core.domain.AjaxResult;
import cn.wolfcode.common.enums.BusinessType;
import cn.wolfcode.store.storePutOut.domain.BusInventoryType;
import cn.wolfcode.store.storePutOut.service.IBusInventoryTypeService;
import cn.wolfcode.common.utils.poi.ExcelUtil;
import cn.wolfcode.common.core.page.TableDataInfo;

/**
 * 出入库管理Controller
 * 
 * @author chen
 * @date 2023-06-16
 */
@RestController
@RequestMapping("/store/storePutOut")
public class BusInventoryTypeController extends BaseController
{
    @Autowired
    private IBusInventoryTypeService busInventoryTypeService;

    /**
     * 查询出入库管理列表
     */
    @PreAuthorize("@ss.hasPermi('store:storePutOut:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusInventoryType busInventoryType)
    {
        startPage();
        List<BusInventoryType> list = busInventoryTypeService.selectBusInventoryTypeList(busInventoryType);
        return getDataTable(list);
    }

    /**
     * 导出出入库管理列表
     */
    @PreAuthorize("@ss.hasPermi('store:storePutOut:export')")
    @Log(title = "出入库管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusInventoryType busInventoryType)
    {
        List<BusInventoryType> list = busInventoryTypeService.selectBusInventoryTypeList(busInventoryType);
        ExcelUtil<BusInventoryType> util = new ExcelUtil<BusInventoryType>(BusInventoryType.class);
        util.exportExcel(response, list, "出入库管理数据");
    }

    /**
     * 获取出入库管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('store:storePutOut:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(busInventoryTypeService.selectBusInventoryTypeById(id));
    }

    /**
     * 新增出入库管理
     */
    @PreAuthorize("@ss.hasPermi('store:storePutOut:add')")
    @Log(title = "出入库管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusInventoryType busInventoryType)
    {
        return toAjax(busInventoryTypeService.insertBusInventoryType(busInventoryType));
    }

    /**
     * 修改出入库管理
     */
    @PreAuthorize("@ss.hasPermi('store:storePutOut:edit')")
    @Log(title = "出入库管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusInventoryType busInventoryType)
    {
        return toAjax(busInventoryTypeService.updateBusInventoryType(busInventoryType));
    }

    /**
     * 删除出入库管理
     */
    @PreAuthorize("@ss.hasPermi('store:storePutOut:remove')")
    @Log(title = "出入库管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(busInventoryTypeService.deleteBusInventoryTypeByIds(ids));
    }


    /**
     * 查询所有的仓库
     */
    @PreAuthorize("@ss.hasPermi('store:storePutOut:listForStore')")
    @GetMapping("/listForStore")
    public AjaxResult listForStore()
    {

        List<BusWarehouseInfo> list = busInventoryTypeService.listForStore();
        return AjaxResult.success(list);
    } /**
     * 查询仓库的单据明细列表
     */
    @PreAuthorize("@ss.hasPermi('store:storePutOut:queryBydocument')")
    @GetMapping("/queryBydocument/{id}")
    public AjaxResult queryBydocument(@PathVariable Long id)
    {

     StoreItemVo list = busInventoryTypeService.queryBydocument(id);
        return AjaxResult.success(list);
    }
    /**
     * 作废
     */
    @PreAuthorize("@ss.hasPermi('store:storePutOut:updataForCrippled')")
    @PostMapping("/updataForCrippled/{id}")
    public AjaxResult updataForCrippled(@PathVariable Long id)
    {

     busInventoryTypeService.updataForCrippled(id);
        return AjaxResult.success();
    }
    /**
     * 入库添加功能
     */
    @PreAuthorize("@ss.hasPermi('store:storePutOut:bePutInStorage')")
    @Log(title = "入库添加", businessType = BusinessType.INSERT)
    @PostMapping("/bePutInStorage")
    public AjaxResult bePutInStorage(@RequestBody StoreItemVo storeItemVo)
    {
        return toAjax(busInventoryTypeService.bePutInStorage(storeItemVo));
    }    /**
     * 出库减少数量功能
     */
    @PreAuthorize("@ss.hasPermi('store:storePutOut:bePutInStorage')")
    @Log(title = "出库添加", businessType = BusinessType.INSERT)
    @PostMapping("/beOutInStorage")
    public AjaxResult beOutInStorage(@RequestBody StoreItemVo storeItemVo)
    {
        return toAjax(busInventoryTypeService.beOutInStorage(storeItemVo));
    }
}
