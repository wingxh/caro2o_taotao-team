package cn.wolfcode.store.storePutOut.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import cn.wolfcode.business.warehouse.domain.BusWarehouseInfo;
import cn.wolfcode.business.warehouse.mapper.BusWarehouseInfoMapper;
import cn.wolfcode.common.utils.DateUtils;
import cn.wolfcode.common.utils.SecurityUtils;
import cn.wolfcode.store.storePutOut.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.wolfcode.store.storePutOut.mapper.BusInventoryTypeMapper;
import cn.wolfcode.store.storePutOut.domain.BusInventoryType;
import cn.wolfcode.store.storePutOut.service.IBusInventoryTypeService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * 出入库管理Service业务层处理
 * 
 * @author chen
 * @date 2023-06-16
 */
@Service
@Transactional
public class BusInventoryTypeServiceImpl implements IBusInventoryTypeService 
{
    @Autowired
    private BusInventoryTypeMapper busInventoryTypeMapper;
    @Autowired
    BusWarehouseInfoMapper busWarehouseInfoMapper;


    /**
     * 查询出入库管理
     * 
     * @param id 出入库管理主键
     * @return 出入库管理
     */
    @Override
    public BusInventoryType selectBusInventoryTypeById(Long id)
    {
        return busInventoryTypeMapper.selectBusInventoryTypeById(id);
    }

    /**
     * 查询出入库管理列表
     * 
     * @param busInventoryType 出入库管理
     * @return 出入库管理
     */
    @Override
    public List<BusInventoryType> selectBusInventoryTypeList(BusInventoryType busInventoryType)
    {
        return busInventoryTypeMapper.selectBusInventoryTypeList(busInventoryType);
    }

    /**
     * 新增出入库管理
     * 
     * @param busInventoryType 出入库管理
     * @return 结果
     */
    @Override
    public int insertBusInventoryType(BusInventoryType busInventoryType)
    {
        busInventoryType.setCreateTime(DateUtils.getNowDate());
        return busInventoryTypeMapper.insertBusInventoryType(busInventoryType);
    }

    /**
     * 修改出入库管理
     * 
     * @param busInventoryType 出入库管理
     * @return 结果
     */
    @Override
    public int updateBusInventoryType(BusInventoryType busInventoryType)
    {
        return busInventoryTypeMapper.updateBusInventoryType(busInventoryType);
    }

    /**
     * 批量删除出入库管理
     * 
     * @param ids 需要删除的出入库管理主键
     * @return 结果
     */
    @Override
    public int deleteBusInventoryTypeByIds(Long[] ids)
    {
        return busInventoryTypeMapper.deleteBusInventoryTypeByIds(ids);
    }

    /**
     * 删除出入库管理信息
     * 
     * @param id 出入库管理主键
     * @return 结果
     */
    @Override
    public int deleteBusInventoryTypeById(Long id)
    {
        return busInventoryTypeMapper.deleteBusInventoryTypeById(id);
    }


    /**
     * 查询所有的仓库列表
     * @return
     */
    @Override
    public List<BusWarehouseInfo> listForStore() {

        return busWarehouseInfoMapper.listForStore();
    }

    /**
     * 查看仓库的单据明细
     * @return
     */
    @Override
    public StoreItemVo queryBydocument(Long id) {
        //拿到出入仓库的id
        BusInventoryType busInventoryType = busInventoryTypeMapper.selectBusInventoryTypeById(id);
        //查询这个id得到数据  判断为不为空
        Assert.notNull(busInventoryType,"非法操作");
       Assert.notNull(busInventoryType.getStore(),"非法操作");
        //根据得到的出入库的id去查询单据明细表中的数据
      List<StoreVo>  storeVos=busInventoryTypeMapper.queryByEntryAndExitByid(id);
        StoreItemVo storeItemVo=new StoreItemVo();
        storeItemVo.setInfo(busInventoryType.getInfo());
        storeItemVo.setName(busInventoryType.getStore());
        storeItemVo.setStoreTime(busInventoryType.getStoreTime());
        storeItemVo.setStoreDocument(storeVos);
        return storeItemVo;
    }

    @Override
    public void updataForCrippled(Long id) {
        //修改作废的状态
        //根据id查询这个数据
        BusInventoryType busInventoryType = busInventoryTypeMapper.selectBusInventoryTypeById(id);
        Assert.notNull(busInventoryType,"非法操作");
        //判断是不是正常的状态
        if (busInventoryType.getCancellationType()!=1){
          throw  new RuntimeException("必须是正常的情况才能作废");
        }
        //查询单据明细中对应的数据集合得到库存中的数据
      List<EntryAndExitDetails>entryAndExitDetails=  busInventoryTypeMapper.queryByEntryAndExitForid(busInventoryType.getId());
        //用这个单据明细中的数量减掉库存中的数据  看能不能减掉  不能就报异常
        for (EntryAndExitDetails entryAndExitDetail : entryAndExitDetails) {
            //查询库存中的信息
            InventoryManagement inventoryManagement=busInventoryTypeMapper
                    .queryByInventoryManagement(entryAndExitDetail.getGoodsId());
            if (inventoryManagement!=null){
                if (busInventoryType.getStoreType()==1){
                    //等于0 就是入库  需要减掉
                    //把库存中的数量减去单据明细中数据数量
                    if (inventoryManagement.getQuantity()-entryAndExitDetail.getQuantity()<0){
                        throw  new RuntimeException("物品库存数量不足，无法执行操作");
                    }
                    //执行sql语句中的数量修改
                    busInventoryTypeMapper.updateByInventoryManagementAndQuantity(inventoryManagement.getId(),entryAndExitDetail.getQuantity());
                }else{
                    busInventoryTypeMapper.AddupdateByInventoryManagementAndQuantity(inventoryManagement.getId(),entryAndExitDetail.getQuantity());
                }

            }
        }
        //是的话改成一
        int CancellationType=0;
        busInventoryTypeMapper.updateByCancellationType(busInventoryType.getId(),CancellationType);

    }

    @Override
    public int bePutInStorage(StoreItemVo storeItemVo) {

        Assert.notNull(storeItemVo,"非法操作");
        Assert.notNull(storeItemVo.getStore(),"必须选择一个仓库");
       if (storeItemVo.getStoreItemVo().size()<0){
           throw  new RuntimeException("必选选择物品才能入库");
       }
       //判断入库时间
        if (new Date().before(storeItemVo.getStoreTime())){
            throw new RuntimeException("入库时间不能超过当天时间");
        }
        //遍历
        //修改数据的数量大小
        //添加入库表中数据  得到出入库中的id
        BusInventoryType busInventoryType=new BusInventoryType();
        busInventoryType.setStoreType(1);
        BusWarehouseInfo busWarehouseInfo = busWarehouseInfoMapper.selectBusWarehouseInfoById(storeItemVo.getStore());
        busInventoryType.setStore(busWarehouseInfo.getWarehouseName());
        busInventoryType.setInputName(SecurityUtils.getUsername());
        busInventoryType.setStoreTime(storeItemVo.getStoreTime());
        busInventoryType.setCreateTime(new Date());
        busInventoryType.setCancellationType(1);
        busInventoryType.setInfo(storeItemVo.getInfo());
        busInventoryTypeMapper.insertBusInventoryType(busInventoryType);
        Long count=0L;  //得到所有物品的数量
        BigDecimal amount=new BigDecimal(0);
        for (StorePutAndOutVo storePutAndOutVo : storeItemVo.getStoreItemVo()) {
            //根据id得到这个物品
            if (storePutAndOutVo!=null){
                InventoryManagement inventoryManagement = busInventoryTypeMapper.queryByInventoryManagement(storePutAndOutVo.getId());
                if (inventoryManagement==null){
                    throw new RuntimeException("没有这个物品");
                }
                //添加库存中的数量
                busInventoryTypeMapper.AddupdateByInventoryManagementAndQuantity(inventoryManagement.getId(),storePutAndOutVo.getQuantity());
                count=count+storePutAndOutVo.getQuantity();
                amount=amount.add(storePutAndOutVo.getSubtotal());
              //设置单据明细中的字段
                EntryAndExitDetails entryAndExitDetails=new EntryAndExitDetails();
                entryAndExitDetails.setInventoryid(busInventoryType.getId());
                entryAndExitDetails.setBrand(inventoryManagement.getBrand());
                entryAndExitDetails.setGoods(inventoryManagement.getName());
                entryAndExitDetails.setOperationtime(new Date());
                entryAndExitDetails.setGoodsId(inventoryManagement.getId());
                entryAndExitDetails.setWarehouse(storeItemVo.getStore());
                entryAndExitDetails.setQuantity((long) storePutAndOutVo.getQuantity());
                entryAndExitDetails.setUnitprice(storePutAndOutVo.getUnitPrice());
                entryAndExitDetails.setType(1L);
                //添加单据明细中的数据
                busInventoryTypeMapper.insertByEntryAndExitDetails(entryAndExitDetails);
            }

        }
        busInventoryType.setGoodsCount(count);
        busInventoryType.setGoodsAmount(amount);
     return    busInventoryTypeMapper.updateBusInventoryType(busInventoryType);

    }

    @Override
    public int beOutInStorage(StoreItemVo storeItemVo) {
        Assert.notNull(storeItemVo,"非法操作");
        Assert.notNull(storeItemVo.getStore(),"必须选择一个仓库");
        if (storeItemVo.getStoreItemVo().size()<0){
            throw  new RuntimeException("必选选择物品才能出库");
        }
        //判断入库时间
        if (storeItemVo.getStoreTime().before(new Date())){
            throw new RuntimeException("出库时间不能超过当天时间");
        }
        //遍历
        //修改数据的数量大小

        //添加入库表中数据  得到出入库管理表的id
        BusInventoryType busInventoryType=new BusInventoryType();
        busInventoryType.setStoreType(0);
        BusWarehouseInfo busWarehouseInfo = busWarehouseInfoMapper.selectBusWarehouseInfoById(storeItemVo.getStore());
        busInventoryType.setStore(busWarehouseInfo.getWarehouseName());
        busInventoryType.setInputName(SecurityUtils.getUsername());
        busInventoryType.setStoreTime(storeItemVo.getStoreTime());
        busInventoryType.setCreateTime(new Date());
        busInventoryType.setCancellationType(1);
        busInventoryType.setInfo(storeItemVo.getInfo());
        busInventoryTypeMapper.insertBusInventoryType(busInventoryType);


        Long count=0L;  //得到所有物品的数量
        BigDecimal amount=new BigDecimal(0);
        for (StorePutAndOutVo storePutAndOutVo : storeItemVo.getStoreItemVo()) {
            //根据id得到这个物品
            if (storePutAndOutVo!=null){
                InventoryManagement inventoryManagement = busInventoryTypeMapper.queryByInventoryManagement(storePutAndOutVo.getId());
               if (inventoryManagement==null){
                   throw new RuntimeException("没有这个物品");
               }
               //判断剩下的数量有没有大于库存中的数量
                if (inventoryManagement.getQuantity()<storePutAndOutVo.getQuantity()){
                    throw new RuntimeException(inventoryManagement.getName()+"物品库存不足");
                }
                //减掉库存中的数量
                busInventoryTypeMapper.updateByInventoryManagementAndQuantity(inventoryManagement.getId(),storePutAndOutVo.getQuantity());
                count=count+storePutAndOutVo.getQuantity();
                amount=amount.add(storePutAndOutVo.getSubtotal());
                //设置单据明细中的字段
                EntryAndExitDetails entryAndExitDetails=new EntryAndExitDetails();
                entryAndExitDetails.setInventoryid(busInventoryType.getId());
                entryAndExitDetails.setBrand(inventoryManagement.getBrand());
                entryAndExitDetails.setGoods(inventoryManagement.getName());
                entryAndExitDetails.setOperationtime(new Date());
                entryAndExitDetails.setGoodsId(inventoryManagement.getId());
                entryAndExitDetails.setWarehouse(storeItemVo.getStore());
                entryAndExitDetails.setQuantity((long) storePutAndOutVo.getQuantity());
                entryAndExitDetails.setUnitprice(storePutAndOutVo.getUnitPrice());
                entryAndExitDetails.setType(0L);
                //添加单据明细中的数据
                busInventoryTypeMapper.insertByEntryAndExitDetails(entryAndExitDetails);
            }

        }




        busInventoryType.setGoodsCount(count);
        busInventoryType.setGoodsAmount(amount);
        return    busInventoryTypeMapper.updateBusInventoryType(busInventoryType);
    }
}
