package cn.wolfcode.store.storePutOut.vo;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Setter
@Getter
public class StorePutAndOutVo {
    private Long id;   //id
    private String name;   //物品
    private BigDecimal unitPrice; //价格
    private Long quantity;   //数量
    private BigDecimal subtotal;  //小计

}
