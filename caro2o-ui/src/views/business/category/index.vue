<template>
  <div class="app-container">
    <el-form :inline="true" :model="queryParams" label-width="68px" ref="queryForm" size="small" v-show="showSearch">
      <el-form-item label="关键字" prop="name">
        <el-input
          @keyup.enter.native="handleQuery"
          placeholder="请输入关键字"
          clearable
          v-model="queryParams.name"
        />
      </el-form-item>

      <el-form-item label="上级分类" prop="id">
        <treeselect v-model="queryParams.id" :options="options" :normalizer="normalizer" placeholder="请选择上级分类" style="width: 230px"/>
      </el-form-item>
      <el-form-item>
        <el-button @click="handleQuery" icon="el-icon-search" size="mini" type="primary">搜索</el-button>
        <el-button @click="resetQuery" icon="el-icon-refresh" size="mini">重置</el-button>
      </el-form-item>
    </el-form>


    <el-row :gutter="10" class="mb8">
      <el-col :span="1.5">
        <el-button
          @click="handleAdd"
          plain
          icon="el-icon-plus"
          size="mini"
          type="primary"
          v-hasPermi="['category:category:add']"
        >添加分类
        </el-button>
      </el-col>
      <el-col :span="1.5">
        <el-button
          @click="handleMove"
          plain
          icon="el-icon-plus"
          size="mini"
          type="primary"
          v-hasPermi="['category:category:move']"
        >物品转移
        </el-button>
      </el-col>

      <right-toolbar :showSearch.sync="showSearch" @queryTable="getList"></right-toolbar>
    </el-row>

    <el-table :data="categoryList" @selection-change="handleSelectionChange" v-loading="loading">
      <el-table-column align="center" type="selection" width="55"/>
      <el-table-column align="center" label="上级分类" prop="parentName"/>
      <el-table-column align="center" label="分类" prop="name"/>
      <el-table-column align="center" label="描述" prop="description"/>
      <el-table-column align="center" class-name="small-padding fixed-width" label="操作">
        <template slot-scope="scope">
          <el-button
            size="mini"
            type="text"
            @click="handleUpdate(scope.row)"
            icon="el-icon-edit"
            v-hasPermi="['category:category:edit']"
          >修改
          </el-button>
          <el-button
            size="mini"
            type="text"
            @click="handleDelete(scope.row)"
            icon="el-icon-delete"
            v-hasPermi="['category:category:remove']"
          >删除
          </el-button>
        </template>
      </el-table-column>
    </el-table>

    <pagination
      :limit.sync="queryParams.pageSize"
      :total="total"
      :page.sync="queryParams.pageNum"
      v-show="total>0"
      @pagination="getList"
    />

    <!-- 添加分类管理对话框 -->
    <el-dialog :title="title" :visible.sync="open" append-to-body width="500px">
      <el-form :model="form" :rules="rules" label-width="80px" ref="form">
        <el-form-item label="上级分类" prop="parentId">
          <el-select clearable v-model="form.parentId">
            <el-option
              :key="item.id"
              :label="item.name"
              :value="item.id"
              v-for="item in this.parents"
            />
          </el-select>
        </el-form-item>
        <el-form-item label="分类名称" prop="name">
          <el-input placeholder="请输入分类名称" v-model="form.name"/>
        </el-form-item>
        <el-form-item label="分类描述" prop="description">
          <el-input placeholder="请输入分类描述" v-model="form.description"/>
        </el-form-item>
      </el-form>
      <div class="dialog-footer" slot="footer">
        <el-button @click="submitForm" type="primary">确 定</el-button>
        <el-button @click="cancel">取 消</el-button>
      </div>
    </el-dialog>

    <!-- 物品转移对话框 -->
    <el-dialog :title="title" :visible.sync="openMove" append-to-body width="500px">
      <el-form :model="move" :rules="rules" label-width="100px" ref="form">
          <el-form-item label="被迁移分类" prop="classifySource">
            <treeselect v-model="move.sourceId" :options="options" :normalizer="normalizer" placeholder="请选择源分类"/>
          </el-form-item>
          <el-form-item label="迁移到" prop="classifyTarget">
            <treeselect v-model="move.targetId" :options="options" :normalizer="normalizer" placeholder="请选择目标分类"/>
          </el-form-item>
          <el-form-item prop="classifyInclude">
            <el-checkbox v-model="move.include">包括子分类</el-checkbox>
          </el-form-item>
      </el-form>
      <div class="dialog-footer" slot="footer">
        <el-button @click="submitForm" type="primary">确 定</el-button>
        <el-button @click="cancelMove">取 消</el-button>
      </div>
    </el-dialog>

    <!-- 修改分类管理对话框 -->
    <el-dialog :title="title" :visible.sync="openUpdate" append-to-body width="500px">
      <el-form :model="form" :rules="rules" label-width="80px" ref="form">
        <el-form-item label="上级分类" prop="parentId">
          <el-select clearable id="" v-model="form.parentId">
            <el-option
              :key="item.id"
              :label="item.name"
              :value="item.id"
              v-for="item in this.parents"
              v-if="form.parentName !== ''"
            />
          </el-select>
        </el-form-item>
        <el-form-item label="分类名称" prop="name">
          <el-input placeholder="请输入分类名称" v-model="form.name"/>
        </el-form-item>
        <el-form-item label="分类描述" prop="description">
          <el-input placeholder="请输入分类描述" v-model="form.description"/>
        </el-form-item>
      </el-form>
      <div class="dialog-footer" slot="footer">
        <el-button @click="submitForm" type="primary">确 定</el-button>
        <el-button @click="cancelUpdate">取 消</el-button>
      </div>
    </el-dialog>

  </div>
</template>

<script>
  import {
    listCategory,
    getCategory,
    delCategory,
    addCategory,
    updateCategory,
    getParentNames,
    getOptions
  } from "@/api/business/category";
  import Treeselect from "@riophae/vue-treeselect";
  import "@riophae/vue-treeselect/dist/vue-treeselect.css";


  export default {
    name: "Category",
    components: { Treeselect },
    data() {
      return {

        params:{
          Name:null,
        },

        // 上级分类选项
        value: [],
        options: [],

        // 上级分类参数
        parents: [],
        // 遮罩层
        loading: true,
        // 数据迁移参数
        move: {},
        // 选中数组
        ids: [],
        // 非单个禁用
        single: true,
        // 非多个禁用
        multiple: true,
        // 显示搜索条件
        showSearch: true,
        // 总条数
        total: 0,
        // 分类管理表格数据
        categoryList: [],
        // 弹出层标题
        title: "",
        // 是否显示添加弹出层
        open: false,
        // 是否显示物品转移弹出层
        openMove:false,
        // 是否显示修改弹出层
        openUpdate: false,
        // 查询参数
        queryParams: {
          pageNum: 1,
          pageSize: 10,
          name: null,
          description: null,
          parentId: null,
          path: null,
          parentName:null ,
          id:null,
        },
        selectValue:"",
        // 表单参数
        form: {},
        // 表单校验
        rules: {
          name: [
            {required: true, message: "分类名称不能为空", trigger: "blur"}
          ],
        }
      };
    },
    created() {
      this.getList();
    },
    methods: {

      /** 转换上级分类数据结构 */
      normalizer(node) {
        if (node.children && !node.children.length) {
          delete node.children;
        }
        return {
          id: node.id,
          label: node.name,
          children: node.children
        };
      },

      handleCascaderChange(value) {
        console.log(value)
        console.log(this.selectValue)
        let newValue = this.selectValue.toString()
        console.log("newValue",newValue)
        this.queryParams.parentName = newValue
      },

      /** 查询分类管理列表 */
      getList() {
        this.loading = true;
        console.log(this.queryParams)
        listCategory(this.queryParams).then(response => {
          this.categoryList = response.rows;
          this.total = response.total;
          this.loading = false;
        });

        getParentNames().then(response => {
          this.parents = response;
        });

        getOptions().then(res => {
          this.options = res;
          console.log(res)
        })
      },
      // 取消增加按钮
      cancel() {
        this.open = false;
        this.reset();
      },

      // 取消增加按钮
      cancelMove() {
        this.openMove = false;
        this.reset();
      },

      // 取消修改按钮
      cancelUpdate() {
        this.openUpdate = false;
        this.reset();
      },
      // 表单重置
      reset() {
        this.form = {
          id: null,
          parentId: null,
          parentName: null,
          name: null,
          description: null,
        };
        this.move = {
          sourceId: null,
          targetId: null,
          include: true,
        };
        this.resetForm("form");
      },
      /** 搜索按钮操作 */
      handleQuery() {
        this.queryParams.pageNum = 1;
        this.getList();
      },
      /** 重置按钮操作 */
      resetQuery() {
        this.resetForm("queryForm");
        this.handleQuery();
      },
      // 多选框选中数据
      handleSelectionChange(selection) {
        this.ids = selection.map(item => item.id)
        this.single = selection.length !== 1
        this.multiple = !selection.length
      },
      /** 新增按钮操作 */
      handleAdd() {
        this.reset();
        this.open = true;
        this.title = "添加分类管理";
      },
      /** 物品转移按钮操作 */
      handleMove() {
        this.reset();
        this.openMove = true;
        this.title = "物品转移";
      },
      /** 修改按钮操作 */
      handleUpdate(row) {
        this.reset();
        const id = row.id || this.ids
        getCategory(id).then(response => {
          this.form = response.data;
          this.openUpdate = true;
          this.title = "修改分类管理";
        });
      },
      /** 提交按钮 */
      submitForm() {
        this.$refs["form"].validate(valid => {
          if (valid) {
            if (this.form.id != null) {
              updateCategory(this.form).then(response => {
                this.$modal.msgSuccess("修改成功");
                this.openUpdate = false;
                this.getList();
              });
            } else {
              addCategory(this.form).then(response => {
                this.$modal.msgSuccess("新增成功");
                this.open = false;
                this.getList();
              });
            }
          }
        });
      },
      /** 删除按钮操作 */
      handleDelete(row) {
        const ids = row.id || this.ids;
        this.$modal.confirm('是否确认删除分类管理编号为"' + ids + '"的数据项？').then(function () {
          return delCategory(ids);
        }).then(() => {
          this.getList();
          this.$modal.msgSuccess("删除成功");
        }).catch(() => {
        });
      },
      /** 导出按钮操作 */
      handleExport() {
        this.download('category/category/export', {
          ...this.queryParams
        }, `category_${new Date().getTime()}.xlsx`)
      }
    }
  };
</script>
