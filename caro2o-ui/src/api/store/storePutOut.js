import request from '@/utils/request'

// 查询出入库管理列表
export function listStorePutOut(query) {
  return request({
    url: '/store/storePutOut/list',
    method: 'get',
    params: query
  })
}

// 查询出入库管理详细
export function getStorePutOut(id) {
  return request({
    url: '/store/storePutOut/' + id,
    method: 'get'
  })
}

// 新增出入库管理
export function addStorePutOut(data) {
  return request({
    url: '/store/storePutOut',
    method: 'post',
    data: data
  })
}

// 修改出入库管理
export function updateStorePutOut(data) {
  return request({
    url: '/store/storePutOut',
    method: 'put',
    data: data
  })
}

// 删除出入库管理
export function delStorePutOut(id) {
  return request({
    url: '/store/storePutOut/' + id,
    method: 'delete'
  })
}
// 查询所有仓库的信息
export function listForStore(query) {
  return request({
    url: '/store/storePutOut/listForStore',
    method: 'get',
    params: query
  })
}
// 查看仓库的单据明细
export function queryBydocument(id) {
  return request({
    url: '/store/storePutOut/queryBydocument/'+id,
    method: 'get',

  })
}
// 查看仓库的单据明细
export function updataForCrippled(id) {
  return request({
    url: '/store/storePutOut/updataForCrippled/'+id,
    method: 'post',

  })
}
// 查看仓库的单据明细
export function bePutInStorage(query) {
  return request({
    url: '/store/storePutOut/bePutInStorage',
    method: 'post',
    data: query
  })
}
// 查看仓库的单据明细
export function beOutInStorage(query) {
  return request({
    url: '/store/storePutOut/beOutInStorage',
    method: 'post',
    data: query
  })
}
