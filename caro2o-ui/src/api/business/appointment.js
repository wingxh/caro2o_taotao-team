import request from '@/utils/request'

// 查询养修信息预约列表
export function listAppointment(query) {
  return request({
    url: '/business/appointment/list',
    method: 'get',
    params: query
  })
}

// 查询养修信息预约详细
export function getAppointment(id) {
  return request({
    url: '/business/appointment/' + id,
    method: 'get'
  })
}

// 新增养修信息预约
export function addAppointment(data) {
  return request({
    url: '/business/appointment',
    method: 'post',
    data: data
  })
}

// 修改养修信息预约
export function updateAppointment(data) {
  return request({
    url: '/business/appointment',
    method: 'put',
    data: data
  })
}

// 删除养修信息预约
export function delAppointment(id) {
  return request({
    url: '/business/appointment/' + id,
    method: 'delete'
  })
}

// 修改到店状态预约
export function arriveStop(id) {
  return request({
    url: '/business/appointment/arriveStop/' + id,
    method: 'put'
  })
}

// 修改取消预约
export function cancelStatus(id) {
  return request({
    url: '/business/appointment/cancelStatus/' + id,
    method: 'put'
  })
}
// 修改取消预约
export function synMsg(id) {
  return request({
    url: '/business/appointment/synMsg/' + id,
    method: 'post'
  })
}

// 结算单按钮
export function appointmentSettle(id) {
  return request({
    url: '/business/appointment/settle/' + id,
    method: 'post'
  })
}

