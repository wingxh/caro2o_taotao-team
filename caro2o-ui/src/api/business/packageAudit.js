import request from '@/utils/request'

// 查询套餐审核列表
export function listPackageAudit(query) {
  return request({
    url: '/business/packageAudit/list',
    method: 'get',
    params: query
  })
}
export function listMyDoneAudit(query) {
  return request({
    url: '/business/packageAudit/myDoneList',
    method: 'get',
    params: query
  })
}export function listMyPackageAudit(query) {
  return request({
    url: '/business/packageAudit/myList',
    method: 'get',
    params: query
  })
}

// 查询套餐审核详细
export function getPackageAudit(id) {
  return request({
    url: '/business/packageAudit/' + id,
    method: 'get'
  })
}

// 新增套餐审核
export function addPackageAudit(data) {
  return request({
    url: '/business/packageAudit',
    method: 'post',
    data: data
  })
}

// 修改套餐审核
export function updatePackageAudit(data) {
  return request({
    url: '/business/packageAudit',
    method: 'put',
    data: data
  })
}

// 删除套餐审核
export function cancelPackageAudit(id) {
  return request({
    url: '/business/packageAudit/cancel/' + id,
    method: 'post'
  })
}

// 查看流程定义明细
export function getAuditProcess(id) {
  return request({
    url: `/business/packageAudit/getAuditProcess/${id}` ,
    method: 'get'
  })
}
// 查看流程定义明细
export function getAuditHistory(id) {
  return request({
    url: `/business/packageAudit/getAuditHistory/${id}` ,
    method: 'get'
  })
}

// 查看流程定义明细
export function auditMyTodo(data) {
  return request({
    url: `/business/packageAudit/auditMyTodo` ,
    method: 'post',
    data : data,
  })
}
