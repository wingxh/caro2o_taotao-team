import request from '@/utils/request'

// 查询合同管理列表
export function listContract(query) {
  return request({
    url: '/business/contract/list',
    method: 'get',
    params: query
  })
}

// 查询合同管理详细
export function getContract(id) {
  return request({
    url: '/business/contract/' + id,
    method: 'get'
  })
}

// 新增合同管理
export function addContract(data) {
  return request({
    url: '/business/contract',
    method: 'post',
    headers : {
      'Content-Type' : 'multipart/form-data'
    },
    data: data
  })
}

// 修改合同管理
export function updateContract(data) {
  return request({
    url: '/business/contract',
    method: 'put',
    headers : {
      'Content-Type' : 'multipart/form-data'
    },
    data: data
  })
}

// 作废合同管理
export function delContract(id) {
  return request({
    url: '/business/contract/cancel/' + id,
    method: 'put'
  })
}

// 合同盖章管理
export function updateStamp(id) {
  return request({
    url: '/business/contract/updateStamp/' + id,
    method: 'put'
  })
}

//审核通过
export function statusSuccess(id) {
  return request({
    url: '/business/contract/statusSuccess/' + id,
    method: 'put'
  })
}

//审核不通过
export function statusFailed(id) {
  return request({
    url: '/business/contract/statusFailed/' + id,
    method: 'put'
  })
}
