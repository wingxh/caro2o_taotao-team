import request from '@/utils/request'

// 查询服务项列表
export function listServiceItem(query) {
  return request({
    url: '/business/serviceItem/list',
    method: 'get',
    params: query
  })
}

// 查询服务项详细
export function getServiceItem(id) {
  return request({
    url: '/business/serviceItem/' + id,
    method: 'get'
  })
}

// 新增服务项
export function addServiceItem(data) {
  return request({
    url: '/business/serviceItem',
    method: 'post',
    data: data
  })
}

// 修改服务项
export function updateServiceItem(data) {
  return request({
    url: '/business/serviceItem',
    method: 'put',
    data: data
  })
}

// 上架服务项
export function saleOn(id) {
  return request({
    url: '/business/serviceItem/saleOn/' + id,
    method: 'put'
  })
}

// 上架服务项
export function saleOff(id) {
  return request({
    url: '/business/serviceItem/saleOff/' + id,
    method: 'put'
  })
}

// 查询服务项发起审核的数据
export function queryAuditInfo(id) {
  return request({
    url: '/business/serviceItem/queryAuditInfo/' + id,
    method: 'get'
  })
}
// 发起审核
export function startServiceItemAudit(param) {
  return request({
    url: '/business/serviceItem/startAudit',
    method: 'post',
    data : param,
  })
}
